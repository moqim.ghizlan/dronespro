function openHideList(div) {
  let ul = div.nextElementSibling;
  if (ul.classList.contains("active")) {
    ul.classList.remove("active");
    div.lastElementChild.classList.remove("fa-caret-up");
    div.lastElementChild.classList.add("fa-caret-down");
  } else {
    ul.classList.add("active");
    div.lastElementChild.classList.remove("fa-caret-down");
    div.lastElementChild.classList.add("fa-caret-up");
  }
}

function openLogoutModal() {
  let modal = document.querySelector(".logout-modal");
  const closeModal = document.querySelector("#closeLogoutModal");
  modal.classList.add("active");
  closeModal.addEventListener("click", () => {
    modal.classList.remove("active");
  });
}
function handelHeaderMenus() {
  const bigMenus = document.querySelector(".big-menu");
  const holder = bigMenus.querySelector("#menuMouseHolder");
  const menu = bigMenus.querySelector("ul");
  const icon = bigMenus.querySelector(".menu-icon");
  const bigMenusDrone = document.querySelector(".drones-menu");
  const holderDrone = bigMenusDrone.querySelector("#droneMenuMouseHolder");
  const aboutUs = document.querySelector("#menu-about-us");
  const aboutUsHolder = aboutUs.querySelector("#aboutUSmenuMouseHolder");
  const aboutUsMenu = aboutUs.querySelector("ul");
  const aboutUsIcon = aboutUs.querySelector(".menu-icon");
  holder.addEventListener("mouseover", () => {
    menu.classList.add("active");
    icon.classList.replace("fa-caret-down", "fa-caret-up");
  });

  holder.addEventListener("mouseout", () => {
    menu.classList.remove("active");
    icon.classList.replace("fa-caret-up", "fa-caret-down");
  });
  menu.addEventListener("mouseover", () => {
    menu.classList.add("active");
    icon.classList.replace("fa-caret-down", "fa-caret-up");
  });

  menu.addEventListener("mouseout", () => {
    menu.classList.remove("active");
    icon.classList.replace("fa-caret-up", "fa-caret-down");
  });

  const menuDrone = bigMenusDrone.querySelector("ul");
  const iconDrone = bigMenusDrone.querySelector(".menu-icon");
  holderDrone.addEventListener("mouseover", () => {
    menuDrone.classList.add("active");
    iconDrone.classList.replace("fa-caret-down", "fa-caret-up");
  });
  holderDrone.addEventListener("mouseout", () => {
    menuDrone.classList.remove("active");
    iconDrone.classList.replace("fa-caret-up", "fa-caret-down");
  });
  menuDrone.addEventListener("mouseover", () => {
    menuDrone.classList.add("active");
    iconDrone.classList.replace("fa-caret-down", "fa-caret-up");
  });
  menuDrone.addEventListener("mouseout", () => {
    menuDrone.classList.remove("active");
    iconDrone.classList.replace("fa-caret-up", "fa-caret-down");
  });

  aboutUsHolder.addEventListener("mouseover", () => {
    aboutUsMenu.classList.add("active");
    aboutUsIcon.classList.replace("fa-caret-down", "fa-caret-up");
  });
  aboutUsHolder.addEventListener("mouseout", () => {
    aboutUsMenu.classList.remove("active");
    aboutUsIcon.classList.replace("fa-caret-up", "fa-caret-down");
  });
  aboutUsMenu.addEventListener("mouseover", () => {
    aboutUsMenu.classList.add("active");
    aboutUsIcon.classList.replace("fa-caret-down", "fa-caret-up");
  });
  aboutUsMenu.addEventListener("mouseout", () => {
    aboutUsMenu.classList.remove("active");
    aboutUsIcon.classList.replace("fa-caret-up", "fa-caret-down");
  });
}

function checkOverflow(el) {
  var curOverflow = el.style.overflow;

  if (!curOverflow || curOverflow === "visible") el.style.overflow = "hidden";

  var isOverflowing =
    el.clientWidth < el.scrollWidth || el.clientHeight < el.scrollHeight;

  el.style.overflow = curOverflow;

  return isOverflowing;
}
