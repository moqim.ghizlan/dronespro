

function addImagesToContainer() {
  const imageInput = document.getElementById("image-input");
  const imageContainer = document.getElementById("image-container");
  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute('type', 'buttun')
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}

addImagesToContainer()