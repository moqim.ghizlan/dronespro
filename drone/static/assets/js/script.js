function changeImagePos() {
  const cards = document.querySelectorAll(".cards-images");
  cards.forEach((card) => {
    card.addEventListener("mouseover", () => {
      card.childNodes[1].classList.add("hidden-img");
      card.childNodes[1].classList.remove("showen-img");
      card.childNodes[1].style.transition = "all 0.5s ease-in-out";
      card.childNodes[3].classList.add("showen-img");
      card.childNodes[3].classList.remove("hidden-img");
      card.childNodes[3].style.transition = "all 0.5s ease-in-out";
    });
    card.addEventListener("mouseout", () => {
      card.childNodes[1].classList.add("showen-img");
      card.childNodes[1].classList.remove("hidden-img");
      card.childNodes[1].style.transition = "all 0.5s ease-in-out";
      card.childNodes[3].classList.add("hidden-img");
      card.childNodes[3].classList.remove("showen-img");
      card.childNodes[3].style.transition = "all 0.5s ease-in-out";
    });
  });
}
function changeImageFocus() {
  const mainImage = document.querySelector("#main-image");
  const Allimages = document.querySelectorAll(".all-images");
  Allimages.forEach((image) => {
    image.addEventListener("mouseover", () => {
      mainImage.src = image.src;
    });
  });
}
function setImageModale() {
  const openModalButton = document.getElementById("main-image");
  if (openModalButton) {
    openModalButton.addEventListener("click", function () {
      const modal = document.getElementById("image-modal");
      const closeButton = document.getElementsByClassName("close")[0];
      const modalImage = document.getElementById("modal-image");
      const sourceImage = document.getElementById("main-image").src;
      modalImage.src = sourceImage;
      modal.style.display = "block";
      closeButton.addEventListener("click", function () {
        modal.style.display = "none";
      });
    });
  }
}
function closeImageOnDoubleClicks() {
  var modal = document.getElementById("image-modal");
  modal.style.display = "none";
}
function drowStars() {
  const starsHolders = document.querySelectorAll(".stars-holder");
  starsHolders.forEach((starHolder) => {
    const starsHolderValue = starHolder.children[0].value;
    const nb = parseInt(starsHolderValue);
    for (let i = 1; i < nb + 1; i++) {
      starHolder.children[i].classList.add("star-checked");
    }
  });
}
function createStar() {
  const starsHolders = document.querySelectorAll(".stars-holder");
  starsHolders.forEach((starHolder) => {
    const starsHolderValue = starHolder.getAttribute("data-stars-holder-value");
    const yallowStars = parseInt(starsHolderValue);
    for (let i = 0; i < 5; i++) {
      const star = document.createElement("span");
      star.classList.add("fa");
      star.classList.add("fa-star");
      starHolder.appendChild(star);
    }
    for (let i = 0; i < yallowStars; i++) {
      starHolder.children[i].classList.add("star-checked");
    }
  });
}
function showRegisteForm() {
  document.getElementById("login").classList.remove("section-active");
  document.getElementById("login").classList.add("section-unactive");
  document.getElementById("registe").classList.remove("section-unactive");
  document.getElementById("registe").classList.add("section-active");
  document.getElementById("loginBtn").classList.remove("btn-active");
  document.getElementById("registeBtn").classList.add("btn-active");
}
function showLoginForm() {
  document.getElementById("registe").classList.remove("section-active");
  document.getElementById("registe").classList.add("section-unactive");
  document.getElementById("login").classList.remove("section-unactive");
  document.getElementById("login").classList.add("section-active");
  document.getElementById("registeBtn").classList.remove("btn-active");
  document.getElementById("loginBtn").classList.add("btn-active");
}
function showHidePassword(button) {
  const password = button.previousElementSibling;
  if (password.type === "password") {
    password.type = "text";
    button.innerHTML = '<i class="fa-solid fa-eye cost-auth-icon"></i>';
  } else {
    password.type = "password";
    button.innerHTML = '<i class="fa-solid fa-eye-slash cost-auth-icon"></i>';
  }
}

function showHidePasswordInputs(btn) {
  const password = btn.previousElementSibling;
  if (password.type === "password") {
    password.type = "text";
    btn.innerHTML =
      '<i class="fa-solid fa-eye cost-auth-icon password-eye"></i>';
  } else {
    password.type = "password";
    btn.innerHTML =
      '<i class="fa-solid fa-eye-slash cost-auth-icon password-eye"></i>';
  }
}
function showDeleteArticleModal() {
  var launchModalButton1 = document.getElementById("launch-modal-delete-1");
  var launchModalButton2 = document.getElementById("launch-modal-delete-2");
  var customModal = document.getElementById("custom-show-info-modal-delete");
  var closeModalButton = document.getElementById(
    "custom-show-info-modal-delete__close-modal"
  );
  var closeModalButton2 = document.querySelector(
    "#custom-show-info-modal-delete__close-modal-bottom"
  );
  launchModalButton1.onclick = function () {
    customModal.style.display = "flex";
  };
  launchModalButton2.onclick = function () {
    customModal.style.display = "flex";
  };
  closeModalButton.onclick = function () {
    customModal.classList.add("custom-show-info-modal__modal-fade-out");
    setTimeout(function () {
      customModal.style.display = "none";
      customModal.classList.remove("custom-show-info-modal__modal-fade-out");
    }, 100);
  };
  closeModalButton2.onclick = function () {
    customModal.classList.add("custom-show-info-modal__modal-fade-out");
    setTimeout(function () {
      customModal.style.display = "none";
      customModal.classList.remove("custom-show-info-modal__modal-fade-out");
    }, 100);
  };
  window.onclick = function (event) {
    if (event.target == customModal) {
      customModal.classList.add("custom-show-info-modal__modal-fade-out");
      setTimeout(function () {
        customModal.style.display = "none";
        customModal.classList.remove("custom-show-info-modal__modal-fade-out");
      }, 200);
    }
  };
}
function closeMessage(span) {
  span.parentElement.classList.remove("active");
}
function openMessage() {
  globalMessage.classList.add("active");
  setTimeout(() => {
    globalMessage.classList.remove("active");
  }, 5000);
}
function setSideBarPos() {
  const headerHeight = document.getElementById("main-header").offsetHeight;
  const sidebarContainer = document.querySelector(".sidebar-container");

  const sidebar = document.querySelector("#sidebar");
  const page = sidebar.dataset.page;
  const section = sidebar.dataset.section;
  const accountCollapse = document.querySelector("#account-collapse");
  const listCollapse = document.querySelector("#list-collapse");
  const myAccount = document.querySelector("#sidebar-myAccount");
  const editAccount = document.querySelector("#sidebar-editAccount");
  const editPassword = document.querySelector("#sidebar-editPassword");
  const editAddress = document.querySelector("#sidebar-editAddress");
  const basket = document.querySelector("#sidebar-basket");
  const favorites = document.querySelector("#sidebar-favorites");
  const repairs = document.querySelector("#sidebar-repairs");
  const orders = document.querySelector("#sidebar-orders");
  const rented = document.querySelector("#sidebar-rented");
  const sidebarAccountBtnColl = document.querySelector(
    "#sidebarAccountBtnColl"
  );
  const sidebarListBtnColl = document.querySelector("#sidebarListBtnColl");

  // sidebarContainer.style.minHeight = `calc(100dvh - ${headerHeight}px)`;

  if (page === "1") {
    accountCollapse.classList.add("show");
    sidebarAccountBtnColl.classList.add("active");
    listCollapse.classList.remove("show");
    sidebarListBtnColl.classList.remove("active");
    if (section === "1") {
      myAccount.classList.remove("text-white");
      myAccount.classList.add("text-blue");
    } else if (section === "2") {
      editAccount.classList.remove("text-white");
      editAccount.classList.add("text-blue");
    } else if (section === "3") {
      editAddress.classList.remove("text-white");
      editAddress.classList.add("text-blue");
    }
  } else if (page === "2") {
    accountCollapse.classList.remove("show");
    sidebarAccountBtnColl.classList.remove("active");
    listCollapse.classList.add("show");
    sidebarListBtnColl.classList.add("active");
    if (section === "1") {
      basket.classList.remove("text-white");
      basket.classList.add("text-blue");
    } else if (section === "2") {
      favorites.classList.remove("text-white");
      favorites.classList.add("text-blue");
    } else if (section === "3") {
      repairs.classList.remove("text-white");
      repairs.classList.add("text-blue");
    } else if (section === "4") {
      orders.classList.remove("text-white");
      orders.classList.add("text-blue");
    } else if (section === "5") {
      rented.classList.remove("text-white");
      rented.classList.add("text-blue");
    }
  }
}
function openSideBar() {
  const sideBar = document.querySelector("#sidebar");
  const openSideBarBtn = document.querySelector("#opensidebar");

  sideBar.classList.add("open");
  openSideBarBtn.classList.add("open");
}
function closeSideBar() {
  const sideBar = document.querySelector("#sidebar");
  const openSideBarBtn = document.querySelector("#opensidebar");

  sideBar.classList.remove("open");
  openSideBarBtn.classList.remove("open");
}
function openCloseRepairInfoClient() {
  const openCloseRepairInfoBtn = document.querySelector(
    "#openCloseRepairInfoBtnClient"
  );
  const openCloseRepairInfoBtnIcon = document.querySelector(
    "#openCloseRepairInfoBtnClientIcon"
  );
  const myRepairInfos = document.querySelector(
    "#my-repair-infos-wapperClient .my-repair-infos"
  );
  if (myRepairInfos.classList.contains("active")) {
    myRepairInfos.classList.remove("active");
    openCloseRepairInfoBtnIcon.classList.remove("fa-arrow-up");
    openCloseRepairInfoBtnIcon.classList.add("fa-arrow-down");
  } else {
    myRepairInfos.classList.add("active");
    openCloseRepairInfoBtnIcon.classList.remove("fa-arrow-down");
    openCloseRepairInfoBtnIcon.classList.add("fa-arrow-up");
  }
}
function openCloseRepairInfoArticle() {
  const openCloseRepairInfoBtn = document.querySelector(
    "#openCloseRepairInfoBtnArticle"
  );
  const openCloseRepairInfoBtnIcon = document.querySelector(
    "#openCloseRepairInfoBtnArticleIcon"
  );
  const myRepairInfos = document.querySelector(
    "#my-repair-infos-wapperArticle .my-repair-infos"
  );
  if (myRepairInfos.classList.contains("active")) {
    myRepairInfos.classList.remove("active");
    openCloseRepairInfoBtnIcon.classList.remove("fa-arrow-up");
    openCloseRepairInfoBtnIcon.classList.add("fa-arrow-down");
  } else {
    myRepairInfos.classList.add("active");
    openCloseRepairInfoBtnIcon.classList.remove("fa-arrow-down");
    openCloseRepairInfoBtnIcon.classList.add("fa-arrow-up");
  }
}
function setStatus() {
  var select = document.getElementById("select-satatus");
  if (select) {
    var status = select.getAttribute("data-status");
    select.value = status;
  } else {
    return;
  }
}
function showDivRefusedReasion(select) {
  const div2 = document.getElementById("div-first_response");
  const div4 = document.getElementById("div-notes");
  const div6 = document.getElementById("div-amount");
  const div8 = document.getElementById("div-tracking_number");
  const div9 = document.getElementById("div-refused_reasion");
  if (select.value == 2 || select.value == "2") {
    div2.classList.add("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
  } else if (select.value == 4 || select.value == "4") {
    div2.classList.remove("active");
    div4.classList.add("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
    return;
  } else if (select.value == 6 || select.value == "6") {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.add("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
    return;
  } else if (select.value == 8 || select.value == "8") {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.add("active");
    div9.classList.remove("active");
    return;
  } else if (select.value == 9 || select.value == "9") {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.add("active");
    return;
  } else {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
    return;
  }
}
function flippeImgs(div) {
  const flipperImgsWapper = div.querySelector(".flipper-imgs-wapper");
  const flipperImgs = flipperImgsWapper.querySelectorAll(".flipper-img");
  const useCase = document.querySelector(".articles-holders-cards-use_case");
  flipperImgsWapper.addEventListener("mouseover", () => {
    flipperImgs[0].style.display = "none";
    flipperImgs[1].style.display = "block";
    useCase.style.display = "none";
  });
  flipperImgsWapper.addEventListener("mouseout", () => {
    flipperImgs[0].style.display = "block";
    flipperImgs[1].style.display = "none";
    useCase.style.display = "block";
  });
}
function sortTableRepairForAdminWithSelect(status) {
  const tableRows = document.querySelectorAll(".table-group-divider tr");
  tableRows.forEach((row) => {
    if (status == 0) {
      row.classList.remove("d-none");
    } else if (row.dataset.status != status) {
      row.classList.add("d-none");
    } else {
      row.classList.remove("d-none");
    }
  });
}
function sortTableProblems(e) {
  const rows = document.querySelectorAll("tbody tr");
  rows.forEach((row) => {
    const status = row.dataset.status;
    if (e == 0) {
      row.style.display = "table-row";
    } else if (e == 1) {
      if (status == 0) {
        row.style.display = "table-row";
      } else {
        row.style.display = "none";
      }
    } else if (e == 2) {
      if (status == 1) {
        row.style.display = "table-row";
      } else {
        row.style.display = "none";
      }
    }
  });
}
function openLinkInNewTab(span) {
  window.open(span.innerText, "", "left=15,top=15,width=800,height=600");
}
function sortTableRepairForAdminWithInput(q) {
  const tableRows = document.querySelectorAll(".table-group-divider tr");
  tableRows.forEach((row) => {
    // show all rows
    row.classList.remove("d-none");
    // hide rows that not match the query
    if (
      !row
        .querySelector("td:nth-child(1)")
        .textContent.toLowerCase()
        .includes(q.toLowerCase()) &&
      !row
        .querySelector("td:nth-child(2)")
        .textContent.toLowerCase()
        .includes(q.toLowerCase())
    ) {
      row.classList.add("d-none");
    }
  });
}
async function applyTypingAnimation(text, element, delay) {
  let stringToWrite = text;
  element.innerHTML = "";
  let refill = "";
  let cursorHTML = "&#128531;";
  for (character of stringToWrite) {
    refill += character;
    await sleep(delay);
    element.innerHTML = refill + cursorHTML;
  }
  cursorHTML = "";
}
function sleep(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
function openCloseAdminSidebar(div) {
  const adminSidebar = document.querySelector("#admin-slidebar");
  const btnOpenCloseAdminSidebarIcon = div.querySelector(
    ".openCloseAdminSidebarIcon"
  );

  const btnOpenClose = div;
  const openCloseHeaderNav = document.querySelector("#header-mobile");
  if (adminSidebar.classList.contains("active")) {
    adminSidebar.classList.remove("active");
    btnOpenCloseAdminSidebarIcon.classList.remove("fa-arrow-left");
    btnOpenCloseAdminSidebarIcon.classList.add("fa-arrow-right");
    div.classList.remove("active");
  } else {
    adminSidebar.classList.add("active");
    btnOpenCloseAdminSidebarIcon.classList.remove("fa-arrow-right");
    btnOpenCloseAdminSidebarIcon.classList.add("fa-arrow-left");
    div.classList.add("active");
  }
  if (openCloseHeaderNav.classList.contains("active")) {
    openCloseHeaderNav.classList.remove("active");
  }
}
function searchControlPanelSections(input) {
  const searchValue = input.value.toLowerCase();
  const sections = document.querySelectorAll(".admin-slidebar-center-sections");
  sections.forEach((section) => {
    const sectionName = section.querySelector(".section-name");
    const sectionNameText = sectionName.innerText.toLowerCase();
    if (sectionNameText.includes(searchValue)) {
      section.classList.add("order-1");
    } else {
      section.classList.remove("order-1");
    }
  });
}
function openCloseSection(section) {
  const sectionId = section.parentElement.id;
  const sectionElement = document.getElementById(sectionId);
  const iconElement = section.querySelector(".row-wapper-icon");
  const sectionName = section.querySelector(".section-name");

  if (sectionElement.classList.contains("active")) {
    sectionElement.classList.remove("active");
    iconElement.classList.remove("fa-caret-up");
    iconElement.classList.add("fa-caret-down");
    sectionName.classList.remove("active");
  } else {
    sectionElement.classList.add("active");
    iconElement.classList.add("fa-caret-up");
    iconElement.classList.remove("fa-caret-down");
    sectionName.classList.add("active");
  }
}

function openCloseHeaderNav(btn) {
  const headerRight = document.querySelector(".header-right");
  const icon = btn.querySelector(".openCloseMenuIcon");

  if (headerRight.classList.contains("active")) {
    headerRight.classList.remove("active");
    icon.classList.add("fa-bars");
    icon.classList.remove("fa-times");
  } else {
    headerRight.classList.add("active");
    icon.classList.add("fa-times");
    icon.classList.remove("fa-bars");
  }
}
function openCloseHeaderMobile(btn) {
  const mobileNav = document.querySelector("#header-mobile");
  const icon = btn.querySelector(".openCloseMenuIcon");
  const adminSidebar = document.querySelector("#admin-slidebar");
  const openAdminSidebar = document.querySelector("#openAdminSidebar");
  if (mobileNav.classList.contains("active")) {
    mobileNav.classList.remove("active");
    icon.classList.remove("revirse");
    icon.classList.add("fa-bars");
    icon.classList.remove("fa-times");
    if (!adminSidebar.classList.contains("active")) {
      adminSidebar.classList.add("active");
      openAdminSidebar.classList.add("active");
    }
  } else {
    mobileNav.classList.add("active");
    icon.classList.add("revirse");
    icon.classList.add("fa-times");
    icon.classList.remove("fa-bars");
    if (adminSidebar.classList.contains("active")) {
      adminSidebar.classList.remove("active");
      openAdminSidebar.classList.remove("active");
    }
  }
}
function openHideList(div) {
  let ul = div.nextElementSibling;
  let i = div.querySelector("i");
  if (ul.classList.contains("active")) {
    ul.classList.remove("active");
    i.classList.remove("revirse");
  } else {
    ul.classList.add("active");
    i.classList.add("revirse");
  }
}

function openSubMenu(li) {
  const subMenu = li.querySelector(".sub-menu");
  const icon = li.querySelector(".sub-menu-icon");
  if (subMenu.classList.contains("active")) {
    subMenu.classList.remove("active");
    icon.classList.add("fa-caret-down");
    icon.classList.remove("fa-caret-up");
  } else {
    subMenu.classList.add("active");
    icon.classList.add("fa-caret-up");
    icon.classList.remove("fa-caret-down");
  }
}
function closeSearcher() {
  const searcher = document.querySelector("#searcher");
  searcher.classList.remove("active");
}

function headerBigDropdownMenu() {
  const menuLeft = document.querySelector(".menu-left");
  const searcher = document.querySelector("#searcher");
  const mouseHolers = document.querySelector(
    "#main-header .menu-left > ul, #main-header .menu-left > ul li"
  );

  mouseHolers.addEventListener("mouseover", function (e) {
    let section = null;
    searcher.classList.add("active");
    if (e.target.classList.contains("header-item")) {
      // stok the value of the item in the section
      section = e.target.id;
    }
  });
  searcher.addEventListener("mouseover", function (e) {
    searcher.classList.add("active");
  });

  menuLeft.addEventListener("mouseout", function (e) {
    searcher.classList.remove("active");
  });
}
function handelModalEvents(button) {
  button.addEventListener("click", () => {
    const trigger = button.getAttribute("data-modal-trigger");
    const modal = document.querySelector(`[data-modal=${trigger}]`);

    const contentWrapper = modal.querySelector(".content-wrapper");
    const close = modal.querySelector(".close");

    close.addEventListener("click", () => modal.classList.remove("open"));
    modal.addEventListener("click", () => modal.classList.remove("open"));
    contentWrapper.addEventListener("click", (e) => e.stopPropagation());

    modal.classList.toggle("open");
  });
}

function handerRepairInputs() {
  const statusSelected = document.querySelector("#repairStatus").dataset.status;
  const div2 = document.getElementById("div-first_response");
  const div4 = document.getElementById("div-notes");
  const div6 = document.getElementById("div-amount");
  const div8 = document.getElementById("div-tracking_number");
  const div9 = document.getElementById("div-refused_reasion");
  if (statusSelected == 2) {
    div2.classList.add("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
  } else if (statusSelected == 4) {
    div2.classList.remove("active");
    div4.classList.add("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
  } else if (statusSelected == 6) {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.add("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
  } else if (statusSelected == 8) {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.add("active");
    div9.classList.remove("active");
  } else if (statusSelected == 9) {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.add("active");
  } else {
    div2.classList.remove("active");
    div4.classList.remove("active");
    div6.classList.remove("active");
    div8.classList.remove("active");
    div9.classList.remove("active");
  }
}

function handerRepaiStaps() {
  const staps = document.querySelectorAll(".stap");
  const currentStap = document.querySelector("#repairStatus").dataset.status;
  const repairStatus = document.querySelector("#repairStatus");

  const finished = repairStatus.dataset.finished;
  if (currentStap === 9 || currentStap === "9") {
    staps.forEach((stap) => {
      stap.classList.add("none");
    });
  } else {
    staps.forEach((stap) => {
      if (stap.dataset.stap == currentStap) {
        stap.classList.add("current");
      }
    });
    // add active to all staps before the current
    const currentStapIndex = currentStap;
    for (let i = 1; i < currentStapIndex; i++) {
      staps.forEach((stap) => {
        if (stap.dataset.stap == i) {
          stap.classList.add("active");
        }
      });
    }

    if (finished == 1) {
      staps.forEach((stap) => {
        if (stap.classList.contains("current")) {
          stap.classList.remove("current");
        }
        stap.classList.add("active");
      });
    }
  }
}

function sortRepairByDate(select) {
  const form = select.parentElement;
  form.submit();
}

function open_close_GM_costum_modal(trigger) {
  var modal = document.getElementById(trigger.dataset.modalTargget);
  var holder = modal.parentElement;
  if (holder.classList.contains("active")) {
    holder.classList.remove("active");
    modal.classList.remove("active");
  } else {
    holder.classList.add("active");
    modal.classList.add("active");
  }
}

function close_GM_modal_btns(btn) {
  var modal = btn.parentElement.parentElement;
  var holder = modal.parentElement;
  holder.classList.remove("active");
  modal.classList.remove("active");
}
function changeCurrentSection(btn) {
  const tagsSection = document.querySelector("#tags-section");
  const typesSection = document.querySelector("#types-section");
  const companiesSection = document.querySelector("#companies-section");

  const btnGoToTags = document.querySelector("#btnGoToTags");
  const btnGoToTypes = document.querySelector("#btnGoToTypes");
  const btnGoToCompanies = document.querySelector("#btnGoToCompanies");
  const gole = btn.dataset.gole;
  switch (gole) {
    case "1":
      tagsSection.classList.add("active");
      typesSection.classList.remove("active");
      companiesSection.classList.remove("active");

      btnGoToTags.classList.add("bg-primary");
      btnGoToTags.classList.remove("bg-secondary");
      btnGoToTypes.classList.remove("bg-primary");
      btnGoToTypes.classList.add("bg-secondary");
      btnGoToCompanies.classList.remove("bg-primary");
      btnGoToCompanies.classList.add("bg-secondary");

      break;
    case "2":
      tagsSection.classList.remove("active");
      typesSection.classList.add("active");
      companiesSection.classList.remove("active");

      btnGoToTags.classList.remove("bg-primary");
      btnGoToTags.classList.add("bg-secondary");
      btnGoToTypes.classList.add("bg-primary");
      btnGoToTypes.classList.remove("bg-secondary");
      btnGoToCompanies.classList.remove("bg-primary");
      btnGoToCompanies.classList.add("bg-secondary");

      break;
    case "3":
      tagsSection.classList.remove("active");
      typesSection.classList.remove("active");
      companiesSection.classList.add("active");

      btnGoToTags.classList.remove("bg-primary");
      btnGoToTags.classList.add("bg-secondary");
      btnGoToTypes.classList.remove("bg-primary");
      btnGoToTypes.classList.add("bg-secondary");
      btnGoToCompanies.classList.add("bg-primary");
      btnGoToCompanies.classList.remove("bg-secondary");

      break;
  }
}
async function addNewTag(btn) {
  let tageName = document.querySelector("#mg-costum-modal-input-1");
  let name = tageName.value;
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const table = document.querySelector("#tags-section-table");
  await fetch("/api/new_tag", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      name,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const tag = data.object;
        let res = `<tr>
                  <td><a href="">#${tag.name}</a></td>
                  <td>
                    <span class="one-ligne-text">${tag.created_at}</span><br><span class="one-ligne-text">${tag.updated_at}</span>
                  </td>
                  <td>
                    <button type="button" onClick="refreshPage()" class="normal-btn shadow py-1 px-2 bg-success text-white mt-2 rounded"><i class="fs-3 fa-solid fa-rotate-right"></i></button>
                  </td>
                </tr>`;
        table.innerHTML += res;
        close_GM_modal_btns(btn);
        tageName.innerHTML = "";
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function refreshPage() {
  window.location.reload();
}
async function addNewCompnay(btn) {
  let compnayName = document.querySelector("#mg-costum-modal-input-3");
  let compnayUrl = document.querySelector("#mg-costum-modal-input-4");
  let name = compnayName.value;
  let site_url = compnayUrl.value;
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const table = document.querySelector("#compnay-section-table");
  await fetch("/api/new_company", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      name,
      site_url,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const compnay = data.object;
        let res = `
                <tr>
                  <td>
                    <a href="">${compnay.name}</a><br>
                    <snap class="fake-link" onclick='window.open(${compnay.site_url}, "_blank");'>${compnay.site_url}</snap>
                  </td>
                  <td>
                    <span class="one-ligne-text">${compnay.created_at}</span><br><span class="one-ligne-text">${compnay.updated_at}</span>
                  </td>
                  <td>
                    <button type="button" onClick="refreshPage()" class="normal-btn shadow py-1 px-2 bg-success text-white mt-2 rounded"><i class="fs-3 fa-solid fa-rotate-right"></i></button>
                  </td>
                </tr>`;
        table.innerHTML += res;
        close_GM_modal_btns(btn);
        compnayName.innerHTML = "";
        compnayUrl.innerHTML = "";
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
async function addNewtype(btn) {
  let articleType = document.querySelector("#mg-costum-modal-input-2");
  let name = articleType.value;
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const table = document.querySelector("#types-section-table");
  await fetch("/api/new_article_type", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      name,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const type = data.object;
        let res = `<tr>
                  <td><a href="">${type.name}</a></td>
                  <td>
                    <span class="one-ligne-text">${type.created_at}</span><br><span class="one-ligne-text">${type.updated_at}</span>
                  </td>
                  <td>
                    <button type="button" onClick="refreshPage()" class="normal-btn shadow py-1 px-2 bg-success text-white mt-2 rounded"><i class="fs-3 fa-solid fa-rotate-right"></i></button>
                  </td>
                </tr>`;
        table.innerHTML += res;
        close_GM_modal_btns(btn);
        articleType.innerHTML = "";
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function handleRepairSection(div) {
  const section = div.parentElement;

  section.classList.toggle("active");
  div.lastElementChild.classList.toggle("revirse");
}

function sortNotesCards(q) {
  const cards = document.querySelectorAll(".other-cards .card");
  cards.forEach((card) => {
    const cardTitle = card.querySelector(".card-title");
    const cardText = card.querySelector(".card-text");
    const cardHeader = card.querySelector(".card-header");
    const cardBody = card.querySelector(".card-body");
    const cardActions = card.querySelector(".other-cards-actions");

    if (
      cardTitle.textContent.toLowerCase().includes(q.toLowerCase()) ||
      cardText.textContent.toLowerCase().includes(q.toLowerCase()) ||
      cardHeader.textContent.toLowerCase().includes(q.toLowerCase()) ||
      cardBody.textContent.toLowerCase().includes(q.toLowerCase())
    ) {
      card.style.display = "block";
    } else {
      card.style.display = "none";
    }
  });
}

function handleAdminQuickAccess() {
  const menu = document.querySelector("#admin-quick-access-menu");
  const btnIcon = document.querySelector("#admin-quick-access-icon");
  menu.classList.toggle("active");
  if (btnIcon.classList.contains("fa-bars")) {
    btnIcon.classList.remove("fa-bars");
    btnIcon.classList.add("fa-times");
  } else {
    btnIcon.classList.remove("fa-times");
    btnIcon.classList.add("fa-bars");
  }
}

function setRentCurrentStatus() {
  const select = document.querySelector("#select-satatus");
  const status = select.getAttribute("data-status");
  select.value = status;
}
function closeMessage(span) {
  span.parentElement.parentElement.style.display = "none";
}

function handleRentShowInputRefusedReasion() {
  const div = document.querySelector("#rent-refused_reasion");
  const select = document.querySelector("#select-satatus");
  const selectValue = select.dataset.status;
  window.addEventListener("DOMContentLoaded", () => {
    if (selectValue == 5 || selectValue == "5") {
      div.classList.add("active");
    } else {
      div.classList.remove("active");
    }
  });
  select.addEventListener("change", (e) => {
    const value = e.target.value;
    if (value == 5 || value == "5") {
      div.classList.add("active");
    } else {
      div.classList.remove("active");
    }
  });
}

function drawStars() {
  var starsContainer = document.querySelectorAll(".stars-container");
  starsContainer.forEach((stars) => {
    var starsNumber = stars.dataset.nb;
    var starsArray = stars.getElementsByTagName("i");
    for (let i = 0; i < starsNumber; i++) {
      starsArray[i].classList.add("fill-color");
    }
  });
}

function handleRentConditions() {
  const conditions = document.querySelector("#conditions");
  const submitBtn = document.querySelector("button[type=submit]");
  conditions.addEventListener("change", () => {
    if (conditions.checked) {
      submitBtn.disabled = false;
    } else {
      submitBtn.disabled = true;
    }
  });
}
function handleRepairConditions() {
  const conditions = document.querySelector("#conditions");
  const submitBtn = document.querySelector("a#okForRepair[disabled]");
  conditions.addEventListener("change", () => {
    if (conditions.checked) {
      submitBtn.disabled = false;
      submitBtn.classList.remove("disabled-link");
    } else {
      submitBtn.disabled = true;
      submitBtn.classList.add("disabled-link");
    }
  });
}

function setTabletrBgColor() {
  const trs = document.querySelectorAll("table.table tbody tr");
  trs.forEach((tr) => {
    const bgcolor = tr.dataset.bgcolor;
    tr.style.backgroundColor = bgcolor;
  });
}

function handleSellUserForm(btn) {
  const form = btn.parentElement;
  const input = form.querySelector("#final-value");
  const data = btn.dataset.value;
  const btns = form.querySelectorAll("button");
  input.value = btn.dataset.value;
  form.submit();
  btns.forEach((btn) => {
    btn.disabled = true;
    btn.innerHTML = "En validation ...";
  });
}

function addImagesToContainersForSliders() {
  const inputs = document.querySelectorAll(".sliders-img");
  const conatiners = document.querySelectorAll(".image-containers");
  //              //

  for (let i = 0; i < inputs.length; i++) {
    let imageContainer = conatiners[i];
    let imageInput = inputs[i];
    imageContainer.style.display = "flex";
    const fileUrls = [];
    imageInput.addEventListener("change", () => {
      imageContainer.innerHTML = "";
      const newFiles = [];
      for (let i = 0; i < imageInput.files.length; i++) {
        const file = imageInput.files[i];
        const dev = document.createElement("div");
        dev.classList.add("image-wrapper");
        const button = document.createElement("button");
        button.classList.add("delete-button");
        button.setAttribute("type", "buttun");
        button.innerHTML = "&times;";
        const image = document.createElement("img");
        image.src = URL.createObjectURL(file);
        image.setAttribute("alt", "image from your device");
        dev.appendChild(image);
        dev.appendChild(button);
        imageContainer.appendChild(dev);
        button.addEventListener("click", () => {
          dev.removeChild(image);
          dev.removeChild(button);
          imageContainer.removeChild(dev);
          fileUrls.splice(fileUrls.indexOf(image.src), 1);
          const fileIndex = Array.from(imageInput.files).findIndex(
            (f) => f.name === file.name
          );
          if (fileIndex !== -1) {
            const newFiles = new DataTransfer();
            Array.from(imageInput.files).forEach((f) => {
              if (f.name !== file.name) {
                newFiles.items.add(f);
              }
            });
            imageInput.files = newFiles.files;
          }
        });
        fileUrls.push(image.src);
        newFiles.push(file);
      }
      imageInput.files = new FileList(newFiles);
    });
  }
}

async function deleteSliderImages(span) {
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const imageId = span.getAttribute("id");
  const divImageWrapper = span.parentElement;

  await fetch(`/api/image/id=${imageId}/delete`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      imageId,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        divImageWrapper.remove();
      }
    })
    .catch((err) => {
      console.log(err);
    });
}
