const sectionPersonalInfo = document.querySelector(".personal-info");
const sectionDroneInfo = document.querySelector(".drone-info");
const sectionDroneImages = document.querySelector(".drone-images");
const btnBack = document.querySelector("#goBack");
const btnAhead = document.querySelector("#goAhead");
const validateBtn = document.querySelector("#validate-btn");
const personalInfoStape = document.querySelector("#personal-info-stape");
const droneInfoStape = document.querySelector("#drone-info-stape");
const droneImagesStape = document.querySelector("#drone-images-stape");
const staps = document.querySelector(".staps");
const inputSection = document.querySelectorAll(".input-section");
const currentStape = inputSection[0];
let currentSection = 1;
const sections = document.querySelectorAll("section");

const input_firstname = document.querySelector("#first_name");
const input_lastname = document.querySelector("#first_name");
const input_email = document.querySelector("#email");
const input_phone = document.querySelector("#phone_number");
const input_adresse = document.querySelector("#adresse");

btnBack.addEventListener("click", goBack);
btnAhead.addEventListener("click", goAhead);

function updateStapColor() {
  if (currentSection === 1) {
    personalInfoStape.classList.add("active-span");
    droneInfoStape.classList.remove("active-span");
    droneInfoStape.classList.remove("passed-span");
    droneImagesStape.classList.remove("active-span");
    droneImagesStape.classList.remove("passed-span");
  }
  if (currentSection === 2) {
    personalInfoStape.classList.add("passed-span");
    personalInfoStape.classList.remove("active-span");
    droneInfoStape.classList.add("active-span");
    droneInfoStape.classList.remove("passed-span");
    droneImagesStape.classList.remove("passed-span");
    droneImagesStape.classList.remove("active-span");
  }
  if (currentSection === 3) {
    droneImagesStape.classList.add("active-span");
    personalInfoStape.classList.add("passed-span");
    droneInfoStape.classList.add("passed-span");
    personalInfoStape.classList.remove("active-span");
    droneInfoStape.classList.remove("active-span");
  }
}

function validateSection(sectionId) {
  var inputs = document.querySelectorAll(
    `#section-${sectionId} input,#section-${sectionId} textarea`
  );
  var isValid = true;

  inputs.forEach(function (input) {
    if (!input.checkValidity()) {
      input.classList.add("invalid-input");
      isValid = false;
    }
  });

  return isValid;
}

function goAhead() {
  if (currentSection < 3) {
    if (validateSection(currentSection)) {
      currentSection++;
      updateSections();
      updateStapColor();
      btnBack.style.display = "block";
      window.scrollTo(0, 0);
    }
  }
  if (currentSection === 3) {
    btnAhead.style.display = "none";
    validateBtn.style.display = "block";
    window.scrollTo(0, 0);
  }
}
function goBack() {
  if (currentSection > 1) {
    currentSection--;
    updateSections();
    updateStapColor();
    btnAhead.style.display = "block";
    validateBtn.style.display = "none";
    window.scrollTo(0, 0);
  }
  if (currentSection === 1) {
    btnBack.style.display = "none";
    window.scrollTo(0, 0);
  }
}

function updateSections() {
  document.querySelectorAll("section").forEach((section) => {
    section.style.display = "none";
  });
  const currentSectionElement = document.getElementById(
    `section-${currentSection}`
  );
  currentSectionElement.style.display = "block";
}

function addImagesToContainer() {
  const imageInput = document.getElementById("image-input");
  const imageContainer = document.getElementById("image-container");
  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute('type', 'buttun')
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
function deleteAllImages() {
  const imageContainer = document.getElementById("image-container");
  while (imageContainer.firstChild) {
    imageContainer.removeChild(imageContainer.firstChild);
  }
}
addImagesToContainer();


function handleSetAddress(){

  const addressInput = document.querySelectorAll(".address-input");
  const addressReview = document.querySelector("#address-review");
  addressInput.forEach((input) => {
    input.addEventListener("input", () => {
      addressReview.value = `${rue_number.value} ${rue_name.value}, ${postal_code.value} ${city.value}, ${country.value}`;
    });
  });
}