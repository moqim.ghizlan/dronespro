
async function set_sections_notifications(token) {
    const repairSectionSpan = document.querySelector('#nb-repair-section-span')
    await fetch("/api/get_admin_notifications", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "X-CSRFToken": token,
      },
    })
    .then((res) => res.json())
    .then((data) => {
        if (data.code === 1) {
            repairSectionSpan.innerHTML = `(${data.nb_repair})`
        }
    })
    .catch((err) => {
        console.log(err);
    });
}
