function addImagesToContainer() {
  const imageInput = document.getElementById("image-input");
  const imageContainer = document.getElementById("image-container");
  imageContainer.classList.remove("d-none");
  imageContainer.classList.add("d-flex");

  const fileUrls = [];
  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
function handleSetAddress() {
  const addressInput = document.querySelectorAll(".address-input");
  const addressReview = document.querySelector("#address-review");
  addressInput.forEach((input) => {
    input.addEventListener("input", () => {
      addressReview.value = `${rue_number.value} ${rue_name.value}, ${postal_code.value} ${city.value}, ${country.value}`;
    });
  });
}



let currentIndex = 0;
const sections = document.querySelectorAll(".sell-section");
const staps = document.querySelectorAll(".stap");
const btns = document.querySelectorAll(".action-btn");
sections[currentIndex].classList.add("active");
staps[currentIndex].classList.add("current");
btns[0].disabled = true;
btns[2].disabled = false;
btns[2].addEventListener("click", () => {
  if (allowToGo(currentIndex)) {
    currentIndex++;
    updateSectionNext(currentIndex);
  }
});
btns[0].addEventListener("click", () => {

    currentIndex--;
    updateSectionPrev(currentIndex);

});

// for (let i = 0; i < staps.length; i++) {
//   staps[i].addEventListener("click", () => {
//     goToSection(i);
//   });
// }
function updateSectionNext(index) {
  sections[index - 1].classList.remove("active");
  sections[index].classList.add("active");
  staps[index - 1].classList.remove("current");
  staps[index - 1].classList.add("passed");
  staps[index].classList.add("current");
  btns[0].disabled = false;
  btns[0].classList.remove("btn-disabled");

  if (index === 0) {
    btns[0].classList.add('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 1) {
    btns[0].classList.remove('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 2) {
    btns[0].classList.remove('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 3) {
    btns[0].classList.remove('d-none');
    btns[1].classList.remove('d-none');
    btns[2].classList.add('d-none');
  }
}
function updateSectionPrev(index) {
  sections[index + 1].classList.remove("active");
  sections[index].classList.add("active");
  staps[index + 1].classList.remove("current");
  staps[index].classList.remove("passed");
  staps[index].classList.add("current");
  btns[2].disabled = false;
  btns[2].classList.remove("btn-disabled");
  btns[1].classList.add("d-none");

  if (index === 0) {
    btns[0].classList.add('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 1) {
    btns[0].classList.remove('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 2) {
    btns[0].classList.remove('d-none');
    btns[1].classList.add('d-none');
    btns[2].classList.remove('d-none');
  } else if (index === 3) {
    btns[0].classList.remove('d-none');
    btns[1].classList.remove('d-none');
    btns[2].classList.add('d-none');
  }
}
// function goToSection(index) {
//   if (!allowToGo(currentIndex)) return
//   sections[currentIndex].classList.remove("active");
//   sections[index].classList.add("active");
//   staps[currentIndex].classList.remove("current");
//   staps[currentIndex].classList.add("passed");
//   staps[index].classList.add("current");
//   currentIndex = index;
//   if (index === 0) {
//     btns[0].classList.add("btn-disabled");
//     btns[0].disabled = true;
//     btns[1].classList.remove("btn-disabled");
//     btns[2].disabled = false;
//     btns[1].classList.add("d-none");
//     staps[0].classList.remove("passed");
//     staps[0].classList.add("current");
//     staps[1].classList.remove("current", "passed");
//     staps[2].classList.remove("current", "passed");
//     staps[3].classList.remove("current", "passed");
//   } else if (index === 1) {
//     btns[0].classList.remove("btn-disabled");
//     btns[0].disabled = false;
//     btns[1].classList.add("d-none");
//     btns[2].disabled = false;
//     btns[2].classList.remove("btn-disabled");
//     staps[0].classList.remove("current");
//     staps[0].classList.add("passed");
//     staps[1].classList.remove("passed");
//     staps[1].classList.add("current");
//     staps[2].classList.remove("current", "passed");
//     staps[3].classList.remove("current", "passed");
//   } else if (index === 2) {
//     btns[0].classList.remove("btn-disabled");
//     btns[0].disabled = false;
//     btns[1].classList.add("d-none");
//     btns[2].disabled = false;
//     btns[2].classList.remove("btn-disabled");
//     staps[0].classList.remove("current");
//     staps[0].classList.add("passed");
//     staps[1].classList.remove("current");
//     staps[1].classList.add("passed");
//     staps[2].classList.add("current");
//     staps[2].classList.remove("passed");
//     staps[3].classList.remove("current", "passed");
//   } else if (index === 3) {
//     btns[0].classList.remove("btn-disabled");
//     btns[0].disabled = false;
//     btns[1].classList.remove("d-none");
//     btns[2].disabled = true;
//     btns[2].classList.add("btn-disabled");
//     staps[0].classList.remove("current");
//     staps[0].classList.add("passed");
//     staps[1].classList.remove("current");
//     staps[1].classList.add("passed");
//     staps[2].classList.remove("current");
//     staps[2].classList.add("passed");
//     staps[3].classList.remove("passed");
//     staps[3].classList.add("passed");
//   } else {
//     return;
//   }

//   if (index === 3) {
//     btns[2].disabled = true;
//     btns[1].classList.remove("d-none");
//     btns[2].classList.add("btn-disabled");
//   } else {
//     btns[2].disabled = false;
//     btns[1].classList.add("d-none");
//     btns[2].classList.remove("btn-disabled");
//   }
//   if (index === 0) {
//     btns[0].disabled = true;
//     btns[0].classList.add("btn-disabled");
//   } else {
//     btns[0].disabled = false;
//     btns[0].classList.remove("btn-disabled");
//   }
// }

function allowToGo(index) {
  let section = sections[index];
  const allInputForThisSection = section.querySelectorAll(
    "input[required], textarea[required]"
  );
  let isValide = true;

  for (let input of allInputForThisSection) {
    if (input.value.trim() === "") {
      input.classList.add("input-required");
      isValide = false;
    }
  }

  return isValide;
}
