const section1 = document.getElementById("section-1");
const section2 = document.getElementById("section-2");
const goNext = document.getElementById("goNext");
const goBack = document.getElementById("goBack");
const addIt = document.getElementById("addIt");
const filesInput = document.getElementById("image-input");
const stap1 = document.querySelector("#stap-1");
const stap2 = document.querySelector("#stap-2");
const btnCreateEleme = document.querySelector("#addDetails");

function addDroneInputCondition() {
  const allNumericInputs = document.querySelectorAll(
    'input[data-acception="only-numbers"]'
  );
  let pattern = /^[0-9.]+$/;
  allNumericInputs.forEach((input) => {
    input.addEventListener("input", (e) => {
      if (!pattern.test(e.target.value)) {
        e.target.value = e.target.value.slice(0, -1);
      }
    });
  });
}

addDroneInputCondition();

goNext.addEventListener("click", () => {
  goToSection2();
});
goBack.addEventListener("click", () => {
  goToSection1();
});
function goToSection2() {
  if (valideSectionOne()) {
    section1.style.display = "none";
    section2.style.display = "block";
    addIt.style.display = "block";
    goNext.style.display = "none";
    goBack.disabled = false;
    stap1.style.color = "green";
    stap2.style.display = "block";
    btnCreateEleme.style.display = "none";
    window.scrollTo(0, 0);
  }
}

function goToSection1() {
  section1.style.display = "block";
  section2.style.display = "none";
  addIt.style.display = "none";
  goNext.style.display = "block";
  goBack.disabled = true;
  stap1.style.color = "black";
  stap2.style.display = "none";
  btnCreateEleme.style.display = "block";
  window.scrollTo(0, 0);
}

function valideSectionOne() {
  const inputs = document.querySelectorAll(
    "#section-1 input, #section-1 textarea, #section-1 select, prices_input"
  );
  const costumInputs = document.querySelectorAll(".costum-input");
  let valide = true;
  inputs.forEach((input) => {
    if (input.required && input.value == "") {
      valide = false;
      input.style.border = "1px solid red";
    }
  });
  costumInputs.forEach((input) => {
    if (input.required && input.value == "") {
      valide = false;
      input.style.border = "1px solid red";
    }
  });
  return valide;
}
filesInput.addEventListener("change", () => {
  addIt.disabled = false;
});

function addImagesToContainer() {
  const imageInput = document.getElementById("image-input");
  const imageContainer = document.getElementById("image-container");
  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}

addImagesToContainer();

function deleteAllImages() {
  const imageContainer = document.getElementById("image-container");
  while (imageContainer.firstChild) {
    imageContainer.removeChild(imageContainer.firstChild);
  }
}

function refreshList() {
  fetch("/api/companies/list")
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const companies = data.objects.companies;
        const companySelect = document.getElementById("company");
        companies.forEach((company) => {
          const option = document.createElement("option");
          option.value = company.id;
          option.innerText = company.name;
          companySelect.appendChild(option);
        });
      }
    })
    .catch((err) => console.log(err));
}

async function addCompany() {
  const modal = document.querySelector("[data-modal=add-compnay]");
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const name = document.getElementById("add-compnay-name").value;
  const site_url = document.getElementById("add-compnay-url").value;
  const errer = document.getElementById("add-compnay-errer");
  if (name === "" || site_url === "") {
    errer.innerHTML = "Remplissez toute les champs !";
    return;
  }
  await fetch("/api/new_company", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      name,
      site_url,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const company = data.object;
        const companySelect = document.getElementById("compnay");
        const option = document.createElement("option");
        option.value = company.id;
        option.innerText = company.name;
        name.innerHTML = "";
        site_url.innerHTML = "";
        companySelect.appendChild(option);
        modal.classList.remove("open");
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

async function addTag() {
  const modal = document.querySelector("[data-modal=add-tag]");

  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const name = document.getElementById("add-tag-name").value;
  const errer = document.getElementById("add-tag-errer");
  if (name === "") {
    errer.innerHTML = "Remplissez toute les champs !";
    return;
  }
  await fetch("/api/new_tag", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      name,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        const tag = data.object;
        const tagSelect = document.getElementById("tags");
        const option = document.createElement("option");
        option.value = tag.id;
        option.innerText = `#${tag.name}`;
        name.innerHTML = "";
        tagSelect.appendChild(option);
        modal.classList.remove("open");
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

async function deleteImage(span) {
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const imageId = span.getAttribute("data-imageId");
  const divImageWrapper = document.querySelector(`#imageId-${imageId}`);

  await fetch(`/api/image/id=${imageId}/delete`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      imageId,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        divImageWrapper.remove();
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

async function deleteTag(select) {
  let tags = document.querySelector("#tags");
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  let tagID = select.getAttribute("id");
  console.log(tags);
  await fetch(`/api/tag/id=${tagID}/delete`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      tagID,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        tags.remove(select);
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

async function deleteDetail(span) {
  const token = document.querySelector('[name="csrfmiddlewaretoken"]').value;
  const detailId = span.getAttribute("data-detailId");
  const divDetailWrapper = document.querySelector(`#detailId-${detailId}`);

  await fetch(`/api/detail/id=${detailId}/delete`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": token,
    },
    body: JSON.stringify({
      detailId,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (data.code === 1) {
        divDetailWrapper.remove();
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function createNewDetails() {
  const section1 = document.querySelector("#section-1");
  const countDetails = section1.getAttribute("data-count-details");
  const inputsDetailsCounter = document.querySelector(
    "#inputsDetailsCounter"
  ).value;
  const p = document.createElement("p");
  p.innerHTML = `
    <div style="gap: 10px; align-content: center; justify-items: center; flex-direction: row;" class="d-flex flex-column flex-lg-row p-1 border rounded">
        <input autocomplete="off" type="text" required name="details-${countDetails}-name" class="costum-input text form-control" id="details-${countDetails}-name"
                label="Ajouter une name à ce détails" placeholder="Ajouter votre détail ici avec son unité | Ex : Le poids (G)">
        <input autocomplete="off" type="text" required name="details-${countDetails}-value" class="costum-input text form-control" id="details-${countDetails}-value"
                label="Ajouter la value de détails" placeholder="Ajouter la valeur ici | Ex : 1500 ">
        <button type="button" onclick="deleteDetails(this)" class="btn btn-danger">Supprimer</button>
    </div>
    `;
  section1.appendChild(p);
  section1.setAttribute("data-count-details", parseInt(countDetails) + 1);
  document.querySelector("#inputsDetailsCounter").value =
    parseInt(inputsDetailsCounter) + 1;
}

function deleteDetails(element) {
  const section1 = document.querySelector("#section-1");
  const countDetails = section1.getAttribute("data-count-details");
  const inputsDetailsCounter = document.querySelector(
    "#inputsDetailsCounter"
  ).value;
  element.parentElement.parentElement.remove();
  section1.setAttribute("data-count-details", parseInt(countDetails) - 1);
  document.querySelector("#inputsDetailsCounter").value =
    parseInt(inputsDetailsCounter) - 1;
}

function handelShowImagesInContainerSection1() {
  const imageInput = document.getElementById("image-section-1-images-input");
  const imageContainer = document.getElementById(
    "image-container-image-section-1"
  );

  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
function handelShowImagesInContainerMainImage() {
  const imageInput = document.getElementById("main-image-input");
  const imageContainer = document.getElementById(
    "main-image-container"
  );

  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
function handelShowImagesInContainerSection2() {
  const imageInput = document.getElementById("image-section-2-images-input");
  const imageContainer = document.getElementById(
    "image-container-image-section-2"
  );

  imageContainer.style.display = "flex";
  const fileUrls = [];

  imageInput.addEventListener("change", () => {
    imageContainer.innerHTML = "";
    const newFiles = [];
    for (let i = 0; i < imageInput.files.length; i++) {
      const file = imageInput.files[i];
      const dev = document.createElement("div");
      dev.classList.add("image-wrapper");
      const button = document.createElement("button");
      button.classList.add("delete-button");
      button.setAttribute("type", "buttun");
      button.innerHTML = "&times;";
      const image = document.createElement("img");
      image.src = URL.createObjectURL(file);
      image.setAttribute("alt", "image from your device");
      dev.appendChild(image);
      dev.appendChild(button);
      imageContainer.appendChild(dev);
      button.addEventListener("click", () => {
        dev.removeChild(image);
        dev.removeChild(button);
        imageContainer.removeChild(dev);
        fileUrls.splice(fileUrls.indexOf(image.src), 1);
        const fileIndex = Array.from(imageInput.files).findIndex(
          (f) => f.name === file.name
        );
        if (fileIndex !== -1) {
          const newFiles = new DataTransfer();
          Array.from(imageInput.files).forEach((f) => {
            if (f.name !== file.name) {
              newFiles.items.add(f);
            }
          });
          imageInput.files = newFiles.files;
        }
      });
      fileUrls.push(image.src);
      newFiles.push(file);
    }
    imageInput.files = new FileList(newFiles);
  });
}
