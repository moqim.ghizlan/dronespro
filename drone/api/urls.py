from django.urls import path
from . import views
urlpatterns = [


    path('get_all_compnay', views.get_all_compnay),
    path('one_compnay/<int:id>', views.one_compnay),
    path('edit_compnay/<int:id>', views.edit_compnay),



    path('get_all_tags', views.get_all_tags),
    path('new_tag', views.new_tag),
    path('one_tag/<int:id>', views.one_tag),
    path('edit_tag/<int:id>', views.edit_tag),
    path('tag/id=<int:id>/delete', views.delete_tag),



    path('get_all_details', views.get_all_details),
    path('new_detail', views.new_detail),


    path('email_exists', views.email_exists),


    path('image/id=<int:id>/delete', views.delete_image),
    path('detail/id=<int:id>/delete', views.delete_detail),





    path('get_admin_notifications', views.get_admin_notifications),



    path('new_article_type', views.new_article_type),
    path('type/id=<int:id>/delete', views.delete_type),


    path('new_company', views.new_company),
    path('company/id=<int:id>/delete', views.delete_company),
]