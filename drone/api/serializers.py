from rest_framework import serializers
from base.models import Article, Company, Tag, Details, Article_type


class ArticleSerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Article
        fields = '__all__'

class CompanySerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Company
        fields = '__all__'

class TagSerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Tag
        fields = '__all__'


class DetailsSerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Details
        fields = '__all__'


class TypeSerializers(serializers.ModelSerializer):
    class Meta(object):
        model = Article_type
        fields = '__all__'