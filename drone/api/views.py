
from rest_framework.response import Response
from rest_framework.decorators import api_view
from base.models import *
from .serializers import ArticleSerializers, CompanySerializers, TagSerializers, DetailsSerializers, TypeSerializers
from django.http import HttpResponse, JsonResponse
@api_view(['POST', "GET"])
def getDate(request):
    articles = Article.objects.all()
    serializer = ArticleSerializers(articles, many=True)
    return Response(serializer.data)

#################################################################################
################################"""  Company  """################################
#################################################################################

@api_view(['POST'])
def new_company(request):
    data = request.data
    company = Company.objects.create(
        name = data['name'],
        site_url = data['site_url']
    )
    serializer = CompanySerializers(company, many=False)
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'ok',
            "object" : serializer.data
         }, safe=False)

@api_view(['GET'])
def get_all_compnay(request):
    compnais = Company.objects.all()
    serializer = CompanySerializers(compnais, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['GET', 'POST'])
def one_compnay(request, id):
    company = Company.objects.filter(id = id).first()
    if not company:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Compnay not found',
         }, safe=False)

    serializer = CompanySerializers(company, many=False)
    return JsonResponse(serializer.data, safe=False)


@api_view(['GET', 'POST'])
def edit_compnay(request, id):
    company = Company.objects.filter(id = id).first()

    if not company:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Compnay not found',
         }, safe=False)
    data = request.data
    company.name = data['name']
    company.site_url = data['site_url']
    company.save()
    serializer = CompanySerializers(company, many=False)
    return JsonResponse(serializer.data, safe=False)


#################################################################################
###################################"""  TAGS  """################################
#################################################################################

@api_view(['GET'])
def get_all_tags(request):
    tags = Tag.objects.all()
    serializer = TagSerializers(tags, many=True)
    return JsonResponse(serializer.data, safe=False)


@api_view(['POST'])
def new_tag(request):
    data = request.data
    tag = Tag.objects.create(
        name = data['name'],
    )
    serializer = TagSerializers(tag, many=False)
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'ok',
            "object" : serializer.data
         }, safe=False)


@api_view(['GET', 'POST'])
def one_tag(request, id):
    tag = Tag.objects.filter(id = id).first()
    if not tag:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Tag not found',
         }, safe=False)

    serializer = TagSerializers(tag, many=False)
    return JsonResponse(serializer.data, safe=False)


@api_view(['GET', 'POST'])
def edit_tag(request, id):
    tag = Tag.objects.filter(id = id).first()

    if not tag:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Tag not found',
         }, safe=False)
    data = request.data
    tag.name = data['name']
    tag.save()
    serializer = TagSerializers(tag, many=False)
    return JsonResponse(serializer.data, safe=False)

@api_view(['POST'])
def delete_tag(request, id):
    tag = Tag.objects.filter(id = id).first()
    if not tag:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'tag not found',
         }, safe=False)
    else:
        tag.delete()
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'tag was deleted',
         }, safe=False)


#################################################################################
################################"""  Details  """################################
#################################################################################

@api_view(['GET'])
def get_all_details(request):
    dtails = Details.objects.all()
    serializer = DetailsSerializers(dtails, many=True)
    return JsonResponse(serializer.data, safe=False)

@api_view(['POST'])
def new_detail(request):
    data = request.data
    details = Details.objects.create(
        name = data['name'],
        value = data['value'],
    )
    serializer = DetailsSerializers(details, many=False)
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'ok',
            "object" : serializer.data
         }, safe=False)




#################################################################################
################################"""  USER  """###################################
#################################################################################
@api_view(['POST'])
def email_exists(request):
    data = request.data
    email = data['email']
    user = User.objects.filter(email = email).exists()
    if user:
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'email exists',
         }, safe=False)
    else:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'email not exists',
         }, safe=False)




#################################################################################
################################"""  IMAGES  """#################################
#################################################################################


@api_view(['POST'])
def delete_image(request, id):
    image = Images.objects.filter(id = id).first()
    print(image)
    if not image:
        print('here 1')
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Images not found',
         }, safe=False)
    else:
        print('here 2')
        image.delete()
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'Image was deleted',
         }, safe=False)
@api_view(['POST'])
def delete_detail(request, id):
    detail = Details.objects.filter(id = id).first()
    if not detail:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'Detail not found',
         }, safe=False)
    else:
        detail.delete()
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'Detail was deleted',
         }, safe=False)



#################################################################################
################################"""  ADMIN  """##################################
#################################################################################

@api_view(['GET'])
def get_admin_notifications(request):
    nb_repair = Repair.objects.filter(active = True, status = 1).count()
    return JsonResponse({
        'code' :  1,
        "nb_repair" : nb_repair,
        }, safe=False
    )



#################################################################################
############################{####"""  TYPE  """##################################
#################################################################################
@api_view(['POST'])
def new_article_type(request):
    data = request.data
    type = Article_type.objects.create(
        name = data['name'],
    )
    serializer = TypeSerializers(type, many=False)
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'ok',
            "object" : serializer.data
         }, safe=False)

@api_view(['POST'])
def delete_type(request, id):
    type = Article_type.objects.filter(id = id).first()
    if not type:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'type not found',
         }, safe=False)
    else:
        type.delete()
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'type was deleted',
         }, safe=False)

@api_view(['POST'])
def delete_company(request, id):
    cpmonay = Company.objects.filter(id = id).first()
    if not cpmonay:
        return JsonResponse(
        {
            'code' :  0,
            "message" : 'cpmonay not found',
         }, safe=False)
    else:
        cpmonay.delete()
        return JsonResponse(
        {
            'code' :  1,
            "message" : 'cpmonay was deleted',
         }, safe=False)
