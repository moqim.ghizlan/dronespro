# DJANDO-FACELOOK

![Image text](https://www.drons.fr/static/images/logo.png)

---

## Bienvenue sur le README de notre site.


### Lancement du projet
* Tout d'abord il vous faut cloner le projet dans le répertoire de votre choix avec la commande:
```
$ git clone https://gitlab.com/moqim.ghizlan/stage-drones-v2
```
* Ensuite il vous faut créer un environnement du travail :
```
$ pip install virtualenv
    ou
$ python3 -m venv env

```
* Ensuite il vous faut activer cette envenvironnement :
```
$ source env/Script/activate
```
* Ensuite RDV dans le dossier du projet :
```
$ cd drones/
```
* La prochaine étape sera d'installer les dépendances :
```
$ pip install -r requirements.txt
```
* A ce stade là, le projet est prêt pour être lancer mais pas la base de données, pour la créer faites,
```
$ python manage.py migrate
```
* Et finalement pour lancer le projet faites :
```
$ python manage.py runserver 127.0.0.1:8000
```
* RDV sur l'URL pour voir le site :
```
$ http://127.0.0.1:8000/
```