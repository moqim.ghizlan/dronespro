from django.core.management.base import BaseCommand, CommandError
from base.models import Company


class Command(BaseCommand):
    def handle(self, *args, **options):
        Company.objects.create(
            name="Test",
            site_url="www.test.com"
        )
        self.stdout.write(self.style.SUCCESS('Data was inserted successfully'))