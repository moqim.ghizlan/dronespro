from django.core.management.base import BaseCommand
from django.utils.text import slugify
from base.models import *


class Command(BaseCommand):
    help = 'Auto-generates slugs for all models'

    def handle(self, *args, **options):
        models_to_update = [Tag]

        for model in models_to_update:
            self.generate_slugs_for_model(model)

        self.stdout.write(self.style.SUCCESS('Slugs generated successfully'))

    def generate_slugs_for_model(self, model):
        objects = model.objects.all()

        for obj in objects:
            slug = slugify(str(obj))  # Generate slug based on the string representation of the object
            unique_slug = self.get_unique_slug(model, slug)
            obj.slug = unique_slug
            obj.save()

    def get_unique_slug(self, model, slug):
        original_slug = slug
        num = 1
        while model.objects.filter(slug=slug).exists():
            slug = f"{original_slug}-{num}"
            num += 1
        return slug