from .models import *
import random
from django.conf import settings




def is_local():
    # return settings.DEBUG
    return False
def get_random_color():
    colors = {
        1 : 'red',
        2 : 'gray',
        3 : 'green',
        4 : 'purple',
        5 : 'brown',
    }
    num1 = random.randint(1, 5)
    return colors[num1]
def get_nb_repairs_tobe_validate():
    try:
        nb =  Repair.objects.filter(active = True, status = 1).count()
    except:
        nb =  0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_sell_tobe_validate():
    try:
        nb =  Sell.objects.filter(active = True, status = 1).count()
    except:
        nb =  0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_commands_tobe_validate():
    try:
        nb =  Command.objects.filter(active = True, status = 1).count()
    except:
        nb =  0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_rent_tobe_validate():
    try:
        nb = Rent.objects.filter(active = True, status = 1).count()
    except:
        nb = 0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_problems():
    try:
        nb = Problem.objects.filter(active = True, resolved = True).count()
    except:
        nb = 0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_messages():
    try:
        nb = Request_message.objects.filter(active = True).count()
    except:
        nb = 0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_total_notes_tobe_validate():
    try:
        nb = Notes.objects.filter(status = 1).count()
    except:
        nb = 0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def get_nb_note_tobe_validate_with_gole(gole):
    try:
        nb = Notes.objects.filter(status = 1, gole = gole).count()
    except:
        nb = 0
    if nb == 0:
        return (False, nb, 'order-2')
    elif nb != 0 and nb > 99:
        return (True, "+99", 'order-1')
    else:
        return (True, nb, 'order-1')
def notification():
    if get_nb_repairs_tobe_validate()[0] or get_nb_sell_tobe_validate()[0] or get_nb_commands_tobe_validate()[0] or get_nb_rent_tobe_validate()[0] or get_nb_problems()[0] or get_nb_messages()[0] or get_total_notes_tobe_validate()[0]:
        return True
    return False

def get_nb_commande_forUser(request):
    if request.user.is_authenticated:
        try:
            nb = Command.objects.filter(user = request.user).count()
        except:
            nb = 0
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)
    return (False, 0)

def get_nb_favorites_forUser(request):
    if request.user.is_authenticated:
        try:
            fa = Favorite.objects.filter(user = request.user).first()
            nb = fa.articles.all().count()
        except:
            nb = 0
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)
    return (False, 0)
def get_nb_basket_forUser(request):
    if request.user.is_authenticated:
        nb = 0
        user = request.user
        basket, created = Basket.objects.get_or_create(user = request.user)
        wishes = Wish.objects.filter(basket = basket).all()
        nb += wishes.count()
        # return nb
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)
    return (False, 0)

def get_nb_repair_forUser(request):
    if request.user.is_authenticated:
        user = request.user
        try:
            nb = Repair.objects.filter(user = user, active = True).count()
        except:
            nb = 0
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)

    return (False, 0)

def get_nb_rent_forUser(request):
    if request.user.is_authenticated:

        user = request.user
        try:
            nb = Rent.objects.filter(user = user, active = True).count()
        except:
            nb = 0
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)

    return (False, 0)

def get_nb_sell_forUser(request):
    if request.user.is_authenticated:
        user = request.user
        try:
            nb = Sell.objects.filter(user = user, active = True).count()
        except:
            nb = 0
        if nb == 0:
            return (False, nb)
        elif nb != 0 and nb > 99:
            return (True, "+99")
        else:
            return (True, nb)

    return (False, 0)

def nb_notification_user(request):
    if request.user.is_authenticated:
        nb = 0
        try:
            nb += get_nb_commande_forUser(request)[1]
            nb += get_nb_favorites_forUser(request)[1]
            nb += get_nb_basket_forUser(request)[1]
            nb += get_nb_repair_forUser(request)[1]
            nb += get_nb_rent_forUser(request)[1]
            nb += get_nb_sell_forUser(request)[1]
        except:
            nb = '!!'
        return nb

    return (False, 0)

def get_all_users():
    return User.objects.all()
def functions(request):
    nb_articles_in_basket = 0
    if request.user.is_authenticated:
        basket, created = Basket.objects.get_or_create(user = request.user)
        nb_articles_in_basket = Wish.objects.filter(basket = basket).count()
        #  = basket.wishes.count()
    repair_status = {
            1 : "En validation",
            2 : "Validée",
            3 : "Colis envoyé",
            4 : "Colis reçu",
            5 : "En réparation",
            6 : "Réparé",
            7 : "Montant réglé",
            8 : "Colis renvoyé",
            9 : "Réfusée",
        }
    rent_status = {
            1 : "En validation",
            2 : "Validée",
            3 : "En location",
            4 : "Location fini",
            5 : "Réfusée",
        }
    command_status = {
            1 : "Commande en validation",
            2 : "Commande validée",
            3 : "Commande réfusé",
            4 : "Commande payée",
            5 : "Commande annulée",
            6 : "Commande envoyée",
            7 : "Commande reçu",
            }
    sell_status = {
            1 : "En étude", # user do the request
            2 : "Validée", # admin accept the request and propose a price
            3 : "Acceptée", # user accepte the price else goto res[11]
            4 : "Colis envoyé par le clinet", #user send the drone
            5 : "Colis reçu par DP", #admin get the drone
            6 : "Tout est OK",#if it's ok, admin confirm the sell else goto res[7]
            7 : "Un problème",#send text to the user
            8 : "Colis renvoyé par DP",#admin resend the drone
            9 : "Colis reçu par le client",#user get his drone
            10 : "Monant réglé", # if not res[7] means all is OK, admin buy the user
            11 : "Réfusé", # if not res[3]
            12 : "Fermée", # if not res[3]
        }
    colors = {
            'gray' : "Grise",
            'black' : "Noire",
            'white' : "Blanche",
            "blue" : "blue",
            "red" : "Rouge",
            "orange" : "Orange",
            'green' : "Vert",
            'yellow' : "Jaune",
            'purple' : "Violette",
            'pink' : "Rose",
            'brown' : "Brun",
            'transparent' : 'Autre'
        }
    return {

        'all_companies' : Company.objects.all(),
        'all_resolution' : Resolution.objects.all(),
        'nb_articles_in_basket' : nb_articles_in_basket,
        'all_atyicle_types' : Article_type.objects.all(),
        'all_tags': Tag.objects.all(),
        'repair_status' : repair_status,
        'command_status' : command_status,
        'sell_status' : sell_status,
        'rent_status' : rent_status,
        'colors' : colors,
        'get_slider_images' : Slider_images.objects.all(),
        'get_index_slider' : index_slider.objects.all(),
        'about_us' : About_us.objects.filter(id = 1).first(),
        'get_random_color' : get_random_color(),
        'get_nb_repairs_tobe_validate':get_nb_repairs_tobe_validate(),
        'get_nb_sell_tobe_validate':get_nb_sell_tobe_validate(),
        'get_nb_rent_tobe_validate' : get_nb_rent_tobe_validate(),
        'get_nb_commands_tobe_validate': get_nb_commands_tobe_validate(),
        'get_nb_problems' : get_nb_problems(),
        'get_nb_messages' : get_nb_messages(),
        'get_total_notes_tobe_validate' : get_total_notes_tobe_validate(),
        'get_nb_notes_site_tobe_validate' : get_nb_note_tobe_validate_with_gole(1),
        'get_nb_notes_article_tobe_validate' : get_nb_note_tobe_validate_with_gole(2),
        'get_nb_notes_repair_tobe_validate' : get_nb_note_tobe_validate_with_gole(3),
        'get_nb_notes_rent_tobe_validate' : get_nb_note_tobe_validate_with_gole(5),
        'get_nb_notes_command_tobe_validate' : get_nb_note_tobe_validate_with_gole(4),
        'notification':notification(),
        'nb_notification_user':nb_notification_user(request),
        'get_nb_commande_foruser' : get_nb_commande_forUser(request),
        'get_nb_favorites_forUser' : get_nb_favorites_forUser(request),
        'get_nb_basket_forUser' : get_nb_basket_forUser(request),
        'get_nb_repair_forUser' : get_nb_repair_forUser(request),
        'get_nb_rent_forUser' : get_nb_rent_forUser(request),
        'get_nb_sell_forUser' : get_nb_sell_forUser(request),
        'get_all_users': get_all_users(),
        'is_local' : is_local()


    }



