# Generated by Django 4.1.1 on 2023-06-15 17:18

import base.models
from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('username', models.CharField(blank=True, max_length=150, null=True)),
                ('first_name', models.CharField(max_length=150)),
                ('last_name', models.CharField(max_length=150)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('phone', models.CharField(blank=True, max_length=150, null=True)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='About_us',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('phone', models.CharField(blank=True, max_length=150, null=True)),
                ('address', models.CharField(blank=True, max_length=250, null=True)),
                ('google_maps_link', models.URLField(blank=True, null=True)),
                ('site_title', models.CharField(default='Drones Pro', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rue_number', models.CharField(blank=True, max_length=150, null=True)),
                ('rue_name', models.CharField(blank=True, max_length=150, null=True)),
                ('postal_code', models.CharField(blank=True, max_length=150, null=True)),
                ('city', models.CharField(blank=True, max_length=150, null=True)),
                ('country', models.CharField(blank=True, max_length=150, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField(max_length=1024)),
                ('description_2', models.TextField(blank=True, max_length=1024, null=True)),
                ('price', models.FloatField()),
                ('reducted', models.BooleanField(default=False)),
                ('quantity', models.IntegerField(default=1)),
                ('is_used', models.BooleanField(default=False)),
                ('is_occasion', models.BooleanField(default=False)),
                ('is_rentable', models.BooleanField(default=False)),
                ('weight', models.CharField(blank=True, max_length=255, null=True)),
                ('height', models.CharField(blank=True, max_length=255, null=True)),
                ('width', models.CharField(blank=True, max_length=255, null=True)),
                ('color', models.CharField(blank=True, max_length=255, null=True)),
                ('speed', models.CharField(blank=True, max_length=255, null=True)),
                ('bat_cap_h', models.CharField(blank=True, max_length=255, null=True)),
                ('flight_time', models.CharField(blank=True, max_length=255, null=True)),
                ('flight_range', models.CharField(blank=True, max_length=255, null=True)),
                ('transmission_range', models.CharField(blank=True, max_length=255, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('rental_amount', models.FloatField(blank=True, null=True)),
                ('rental_accessories', models.TextField(blank=True, max_length=1024, null=True)),
                ('added_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='drone_added_by', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_at', '-updated_at'],
            },
        ),
        migrations.CreateModel(
            name='Article_type',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('nb_article', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Basket',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Basket_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Command',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('command_number', models.CharField(blank=True, default=base.models.create_new_ref_number, editable=False, max_length=10, null=True, unique=True)),
                ('status', models.IntegerField(default=1)),
                ('further_details', models.TextField(blank=True, max_length=1024, null=True)),
                ('tracking_number', models.CharField(blank=True, max_length=255, null=True)),
                ('total_amount', models.FloatField(blank=True, null=True)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('site_url', models.URLField(blank=True, null=True)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Details',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(max_length=255)),
                ('value', models.TextField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Iframe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('description', models.TextField(blank=True, max_length=1024, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.ImageField(upload_to='repair_images/')),
            ],
        ),
        migrations.CreateModel(
            name='Problem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('url', models.URLField()),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField(max_length=1024)),
                ('resolved', models.BooleanField(default=False)),
                ('user_email', models.EmailField(blank=True, max_length=254, null=True)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['resolved', '-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Resolution',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150)),
                ('height', models.IntegerField()),
                ('width', models.IntegerField()),
                ('created_at', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Slider_images',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.ImageField(upload_to='slider_images/')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Wish',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('quantity', models.IntegerField(default=1)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='article_in_wish', to='base.article')),
                ('basket', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Wish_in_basket', to='base.basket')),
            ],
        ),
        migrations.CreateModel(
            name='Sell',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('number', models.CharField(blank=True, default=base.models.create_new_ref_number, editable=False, max_length=10, null=True, unique=True)),
                ('status', models.IntegerField(default=1)),
                ('stat', models.IntegerField(default=1)),
                ('serie', models.CharField(max_length=255)),
                ('active', models.BooleanField(default=True)),
                ('readed', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=150)),
                ('description', models.TextField(max_length=1024)),
                ('accessories', models.TextField(max_length=1024)),
                ('first_price', models.FloatField(blank=True, null=True)),
                ('final_price', models.FloatField(blank=True, null=True)),
                ('finished', models.BooleanField(default=False)),
                ('rated', models.BooleanField(default=False)),
                ('tracking_number', models.CharField(blank=True, max_length=255, null=True)),
                ('user_iban', models.CharField(blank=True, max_length=255, null=True)),
                ('refused_reasion', models.CharField(blank=True, max_length=255, null=True)),
                ('article_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='article_type_sell', to='base.article_type')),
                ('compnay', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='compnay_sell', to='base.company')),
                ('images', models.ManyToManyField(blank=True, to='base.images')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_sell', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['status', '-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('description', models.TextField(blank=True, max_length=1024, null=True)),
                ('image', models.ManyToManyField(blank=True, to='base.images')),
            ],
        ),
        migrations.CreateModel(
            name='Request_message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('request', models.TextField(max_length=2024)),
                ('answer', models.TextField(blank=True, max_length=2024, null=True)),
                ('readed', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True)),
                ('images', models.ManyToManyField(blank=True, to='base.images')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_request_message', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['readed', '-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Repair',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('readed', models.BooleanField(default=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('drone_siret', models.CharField(max_length=255)),
                ('drone_damages', models.CharField(blank=True, max_length=255)),
                ('status', models.IntegerField(default=1)),
                ('repair_number', models.CharField(blank=True, default=base.models.create_new_ref_number, editable=False, max_length=10, null=True, unique=True)),
                ('refused_reasion', models.CharField(blank=True, max_length=255, null=True)),
                ('tracking_number', models.CharField(blank=True, max_length=255, null=True)),
                ('amount', models.FloatField(blank=True, null=True)),
                ('first_response_amount', models.CharField(blank=True, max_length=255)),
                ('first_response_duration', models.CharField(blank=True, max_length=255)),
                ('active', models.BooleanField(default=True)),
                ('finished', models.BooleanField(default=False)),
                ('rated', models.BooleanField(default=False)),
                ('notes', models.CharField(blank=True, max_length=255, null=True)),
                ('images', models.ManyToManyField(blank=True, to='base.images')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Repair_drone_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['status', '-readed', '-created'],
            },
        ),
        migrations.CreateModel(
            name='Rent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=1)),
                ('start_at', models.DateField(blank=True, null=True)),
                ('end_at', models.DateField(blank=True, null=True)),
                ('number', models.CharField(blank=True, default=base.models.create_new_ref_number, editable=False, max_length=10, null=True, unique=True)),
                ('explanation', models.TextField(max_length=1024)),
                ('active', models.BooleanField(default=True)),
                ('refused_reasion', models.CharField(blank=True, max_length=255, null=True)),
                ('first_response', models.CharField(blank=True, max_length=255)),
                ('finished', models.BooleanField(default=False)),
                ('rated', models.BooleanField(default=False)),
                ('appointment_at', models.CharField(blank=True, max_length=255, null=True)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Article_Rent', to='base.article')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='User_Rent', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['status', '-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Price_reducted',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('old_price', models.FloatField()),
                ('new_price', models.FloatField()),
                ('start_at', models.DateField(blank=True, null=True)),
                ('end_at', models.DateField(blank=True, null=True)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Price_reducted', to='base.article')),
            ],
        ),
        migrations.CreateModel(
            name='Notes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stars', models.IntegerField(default=5)),
                ('note', models.TextField(blank=True, max_length=1024, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=1)),
                ('gole', models.IntegerField(default=1)),
                ('article', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='article_note', to='base.article')),
                ('command', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='command_note', to='base.command')),
                ('rent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='rent_note', to='base.rent')),
                ('repair', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='repair_note', to='base.repair')),
                ('sell', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sell_note', to='base.sell')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_note', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['status', '-created_at'],
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=1)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='item_article', to='base.article')),
            ],
        ),
        migrations.CreateModel(
            name='index_slider',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(blank=True, null=True)),
                ('image', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='index_slider_image', to='base.images')),
            ],
        ),
        migrations.CreateModel(
            name='Favorite',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('articles', models.ManyToManyField(blank=True, to='base.article')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Favorite_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='command',
            name='items',
            field=models.ManyToManyField(blank=True, to='base.item'),
        ),
        migrations.AddField(
            model_name='command',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='command_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='article',
            name='article_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='article_type', to='base.article_type'),
        ),
        migrations.AddField(
            model_name='article',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='drone_manufacturer', to='base.company'),
        ),
        migrations.AddField(
            model_name='article',
            name='details',
            field=models.ManyToManyField(blank=True, to='base.details'),
        ),
        migrations.AddField(
            model_name='article',
            name='iframe',
            field=models.ManyToManyField(blank=True, to='base.iframe'),
        ),
        migrations.AddField(
            model_name='article',
            name='images',
            field=models.ManyToManyField(blank=True, to='base.images'),
        ),
        migrations.AddField(
            model_name='article',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='article_mainImage', to='base.images'),
        ),
        migrations.AddField(
            model_name='article',
            name='resolution',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='resolution', to='base.resolution'),
        ),
        migrations.AddField(
            model_name='article',
            name='sections',
            field=models.ManyToManyField(blank=True, to='base.section'),
        ),
        migrations.AddField(
            model_name='article',
            name='tags',
            field=models.ManyToManyField(blank=True, to='base.tag'),
        ),
        migrations.AddField(
            model_name='user',
            name='address',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_address', to='base.address'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.permission', verbose_name='user permissions'),
        ),
    ]
