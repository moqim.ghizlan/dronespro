# Generated by Django 4.1.1 on 2023-06-19 16:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0012_rename_company_repair_compnay'),
    ]

    operations = [
        migrations.RenameField(
            model_name='repair',
            old_name='compnay',
            new_name='company',
        ),
    ]
