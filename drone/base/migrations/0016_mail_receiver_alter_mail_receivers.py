# Generated by Django 4.1.1 on 2023-06-23 19:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0015_mail'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mail_receiver',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
            ],
        ),
        migrations.AlterField(
            model_name='mail',
            name='receivers',
            field=models.ManyToManyField(related_name='mail_to', to='base.mail_receiver'),
        ),
    ]
