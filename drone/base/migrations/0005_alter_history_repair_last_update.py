# Generated by Django 4.1.1 on 2023-06-18 19:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0004_history_repair'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history_repair',
            name='last_update',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
