# Generated by Django 4.1.1 on 2023-06-19 15:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0009_repair_end_at_repair_start_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sell',
            name='article_type',
        ),
        migrations.AddField(
            model_name='repair',
            name='article_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='article_type_sell', to='base.article_type'),
        ),
    ]
