from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from .models import *
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm
User = get_user_model()

class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "password1", "password2")
        labels = {
            'first_name': _('Prénom'),
            'last_name': _('Nom'),
            'email': _('Adresse mail'),
            'password1': _('Mot de passe'),
            'password2': _('Confirmation du mot de passe'),
            }
    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user



class EmailAuthenticationForm(AuthenticationForm):
    email = forms.EmailField(
        max_length=254,
        widget=forms.TextInput(
            attrs={
                'autofocus': True,
                'class': 'form-control',
                'placeholder': 'Ex : jon.snow@gmail.com',

                })
    )



class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField(
        required = True,
        widget = forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Ex : jon.snow@gmail.com',
                }
            )
        )

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2')
        widgets = {
            'first_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Ex : Jon',
            }),
            'last_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Ex : SNOW',
            }),
            'password1': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': '************',
            }),
            'password2': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': '************',
            }),
        }



class DroneForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = '__all__'




# class RepairForm(forms.ModelForm):
#     class Meta:
#         model = Repair
#         fields = (
#             'user_phone',
#             'user_address',
#             'drone_siret',
#             'drone_damages',
#             )
#         widgets = {
#             'user_phone':  forms.TextInput(
#                 attrs={
#                     'class': 'text form-control cost-repair-form',
#                     'id' : 'user_phone',
#                     'required' : True,
#                     'label' : 'Votre numéro de téléphone'
#                     }),
#             'user_address':  forms.TextInput(
#                 attrs={
#                     'class': 'text form-control cost-repair-form',
#                     'id' : 'user_address',
#                     'required' : True,
#                     'label' : 'Votre numéro de téléphone'
#                     }),
#             'drone_siret':  forms.TextInput(
#                 attrs={
#                     'class': 'text form-control cost-repair-form',
#                     'id' : 'drone_siret',
#                     'required' : True,
#                     'label' : 'SIRET de votre drone'
#                     }),
#             'drone_damages':  forms.Textarea(
#                 attrs={
#                     'class': 'text form-control cost-repair-form',
#                     'id' : 'drone_damages',
#                     'required' : True,
#                     'label' : 'Détails des dégats',
#                     'row' : 5,
#                     }),

#                 }
#         labels = {
#             'user_phone': _('Numéro de téléphone'),
#             'user_address': _('Adresse'),
#             'drone_siret': _('SIRET de drone'),
#             'drone_damages': _('Détails des dégats'),
#             }
