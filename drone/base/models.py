from django.db import models
from django.contrib.auth.models import AbstractUser
import uuid
from django.core.mail import send_mail
from django.conf import settings
import random
from django.utils import timezone
from datetime import date
# from base.manager import ArticleManager
from django.utils.text import slugify

class ArticleManager(models.Manager):
    def get_queryset(self):
        articles = super().get_queryset()
        expired_price_reducted_ids = Price_reducted.objects.filter(end_at__lt=date.today()).values_list('article_id', flat=True)
        articles.filter(id__in=expired_price_reducted_ids).update(reducted=False)
        return articles

def get_random_color():
    colors = {
        1 : 'red',
        2 : 'gray',
        3 : 'green',
        4 : 'purple',
        5 : 'brown',
        6 : 'orang',
        7 : 'blue',
    }
    num1 = random.randint(1, 7)
    return colors[num1]

def create_new_ref_number():
      return str(random.randint(1000000000, 9999999999))


class About_us(models.Model):
    email = models.EmailField(null=False, blank=False)
    phone = models.CharField(max_length=150, null=True, blank=True)
    address = models.CharField(max_length=250, null=True, blank=True)
    google_maps_link = models.URLField(blank=True, null=True)
    site_title = models.CharField(max_length=255, default="Drones Pro", blank=False, null=False)

    def __str__(self):
        return self.email

class Tag(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name
    def get_color(self):
        return get_random_color()

class Address(models.Model):
    rue_number = models.CharField(max_length=150, null=True, blank=True)
    rue_name = models.CharField(max_length=150, null=True, blank=True)
    postal_code = models.CharField(max_length=150, null=True, blank=True)
    city = models.CharField(max_length=150, null=True, blank=True)
    country = models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return f'{self.rue_number} {self.rue_name} {self.postal_code} {self.city} {self.country}'

    def get_address(self):
        return f'{self.rue_number} {self.rue_name}, {self.postal_code} {self.city}, {self.country}'

class User(AbstractUser):
    username = models.CharField(max_length=150, null=True, blank=True, unique=False)
    first_name = models.CharField(max_length=150, null=False)
    last_name = models.CharField(max_length=150, null=False)
    email = models.EmailField(max_length=254, null=False, unique=True)
    phone = models.CharField(max_length=150, null=True, blank=True)
    address = models.OneToOneField(
        Address,
        on_delete=models.CASCADE,
        related_name="user_address",
        blank=True,
        null=True
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def get_init_name(self):
        return f'{self.last_name[0]}.{self.first_name[0]}'
    def get_rights(self):
        return 'Admin' if self.is_superuser else 'Utilisateur'
    def get_last_name(self):
        return self.last_name.upper()
    def get_first_name(self):
        return self.first_name.title()

class Company(models.Model):
    name = models.CharField(max_length=150, null=False)
    site_url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True)
    def __str__(self):
        return f'{self.name}'
    def get_site_url(self):
        if len(self.site_url) > 20:
            return f'{self.site_url[:20]}...'
        return self.site_url
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Company, self).save(*args, **kwargs)

class Resolution(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    height = models.IntegerField(blank=False, null=False)
    width = models.IntegerField(blank=False, null=False)
    created_at = models.DateTimeField(default=timezone.now, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return f'{self.name}  {self.height}x{self.width}'
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Resolution, self).save(*args, **kwargs)

class Images(models.Model):
    url = models.ImageField(upload_to = 'repair_images/')
    # slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return f'{self.id}'
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.url, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Images, self).save(*args, **kwargs)

class Details(models.Model):
    name =  models.TextField(max_length=255, blank=False, null=False)
    value  =  models.TextField(max_length=255, blank=False, null=False)
    # slug = models.SlugField(null=True, blank=True, unique=True)
    def __str__(self):
        return self.name
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Details, self).save(*args, **kwargs)

class Article_type(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    nb_article = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True, unique=True)
    def __str__(self):
        return f'{self.name}'
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Article_type, self).save(*args, **kwargs)

class Iframe(models.Model):
    url = models.URLField(null=False, blank=False)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(max_length=1024, blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True, unique=True)

    def __str__(self):
        return 'iframe'
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Iframe, self).save(*args, **kwargs)
class Section(models.Model):
    image = models.ManyToManyField(Images, blank=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(max_length=1024, blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True, unique=True)
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.name, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Section, self).save(*args, **kwargs)

    def get_title(self):
        return self.title if self.title else ""

    def is_valid(self):
        if self.title !="" and self.description !="" and self.image.count()>0:
            return True
        else:
            return False
    def get_description(self):
        return self.description if self.description else ""



    def __str__(self):
        return 'section'

class Article(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=150, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=False, null=False)
    description_2 = models.TextField(max_length=1024, blank=True, null=True)
    price = models.FloatField(blank=False, null=False)
    details = models.ManyToManyField(Details, blank=True)
    reducted = models.BooleanField(default=False)
    images = models.ManyToManyField(Images, blank=True)
    iframe = models.ManyToManyField(Iframe, blank=True)
    quantity = models.IntegerField(default=1)
    is_used = models.BooleanField(default=False)
    is_occasion = models.BooleanField(default=False)
    is_rentable = models.BooleanField(default=False)
    tags = models.ManyToManyField(Tag, blank=True)
    weight = models.CharField(max_length=255, blank=True, null=True)
    height = models.CharField(max_length=255, blank=True, null=True)
    width = models.CharField(max_length=255, blank=True, null=True)
    color = models.CharField(max_length=255, blank=True, null=True)

    speed = models.CharField(max_length = 255, blank=True, null=True)
    bat_cap_h = models.CharField(max_length = 255, blank=True, null=True)
    flight_time = models.CharField(max_length = 255, blank=True, null=True)
    flight_range = models.CharField(max_length = 255, blank=True, null=True)
    transmission_range = models.CharField(max_length = 255, blank=True, null=True)
    is_active = models.BooleanField(default= True)

    rental_amount = models.FloatField(blank=True, null=True)
    rental_accessories = models.TextField(max_length=1024, blank=True, null=True)


    sections = models.ManyToManyField(Section, blank=True)
    objects = ArticleManager()


    main_image = models.ForeignKey(Images, on_delete=models.SET_NULL, related_name='article_mainImage', blank=True, null=True)
    added_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="drone_added_by")
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="drone_manufacturer")
    article_type = models.ForeignKey(Article_type, on_delete=models.CASCADE, related_name="article_type")
    resolution = models.ForeignKey(Resolution, on_delete=models.CASCADE, related_name="resolution")
    # slug = models.SlugField(null=True, blank=True, unique=True)
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.title, self.id)
    #     self.slug = slugify(slug_str)
    #     super(Article, self).save(*args, **kwargs)
    class Meta:
            ordering = ['-created_at', '-updated_at']
    def get_rental_amount(self):
        if self.rental_amount:
            return self.rental_amount
        return ""
    def get_20_title(self):
        if len(self.title) > 20:
            return f'{self.title[:20]}..'
        return self.title
    def get_rental_accessories(self):
        if self.rental_accessories:
            return self.rental_accessories
        return ""
    def __str__(self):
        return f'{self.title}'
    def use_case(self):
        if self.is_used:
            return (1, "Reconditionné")
        else:
            return (0, "Neuf")
    def is_occ(self):
        return (1, "Occasion") if self.is_occasion else None
    def get_cases(self):
        cas1 = (self.is_rentable, 'Location', "blue")
        cas2 = None
        if self.is_occasion:
            cas2 = (self.is_occasion, 'Occasion', "green")
        if not self.is_occasion and self.is_used:
            cas2 = (self.is_used, 'Recond...', "orang")
        if not self.is_occasion and not self.is_used:
            cas2 = (True, 'Neuf', "red")
        return [cas1, cas2]
    def get_article_stat(self):
        res = list()
        if self.is_occasion:
            res.append('Occasion')
        if self.is_used:
            res.append('Reconditionné')
        if not self.is_used and not self.is_occasion:
            res.append('Neuf')
        return res

    def get_simple_title(self):
        if len(self.title) > 20:
            return self.title[:20] + "..."
        return self.title

    def get_red_startat(self):
        if self.reducted:
            red = Price_reducted.objects.filter(article = self).first()
            return red.start_at
    def get_red_endat(self):
        if self.reducted:
            red = Price_reducted.objects.filter(article = self).first()
            return red.end_at
    def get_red_price(self):
        if self.reducted:
            red = Price_reducted.objects.filter(article = self).first()
            return red.new_price


    def get_price_frnce_format(self):
        return '{:,.2f}'.format(self.price).replace(',', ' ').replace('.', ',') + ' €'
    def set_description_format(self):
        res = ""
        if len(self.description) > 300:
            res = self.description[:300] + "..."
        else:
            res = self.description
        return res
    def is_more_the_400(self):
        return True if len(self.description) > 300 else False
    def in_stock(self):
        return True if self.quantity > 0 else False
    def get_article_type_fr(self):
        res = {
            1: 'Drones de loisir',
            2: 'Drones professionnels',
            3: 'Piéces détachées',
            4: 'Caméra stabilisée',
            5: 'Accessoires',
        }
        return res[self.article_type.id]
    def description_50(self):
        res = ""
        if len(self.description) > 50:
            res = self.description[:50] + "..."
        else:
            res = self.description
        return res
    def title_45(self):
        res = ""
        if len(self.title) > 45:
            res = self.title[:45] + "..."
        else:
            res = self.title
        return res

class Rent(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    article = models.ForeignKey(Article, related_name="Article_Rent", on_delete=models.CASCADE)
    status = models.IntegerField(default=1)
    user = models.ForeignKey(User, related_name='User_Rent', on_delete=models.CASCADE)
    start_at = models.DateField(blank=True, null=True)
    end_at = models.DateField(blank=True, null=True)
    number = models.CharField(max_length = 10, blank=True, null=True, editable=False, unique=True, default=create_new_ref_number)
    explanation = models.TextField(max_length=1024, blank=False, null=False)
    active = models.BooleanField(default=True)
    refused_reasion = models.CharField(max_length=255, blank=True, null=True)
    first_response = models.CharField(max_length=255, blank=True)
    finished = models.BooleanField(default=False)
    rated = models.BooleanField(default=False)
    appointment_at = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.article}'
    def calcul_total_price(self):
        delta = self.end_at - self.start_at
        res = delta.days * self.article.rental_amount
        return '{:,.2f}'.format(res).replace(',', ' ').replace('.', ',') + ' €'
    def get_total_time(self):
        delta = self.end_at - self.start_at
        return delta.days
    def get_visibility(self):
        return 'Visible' if self.active else 'Archivée'
    def get_franch_phrase(self):
        res = {
            1 : "En validation",
            2 : "Validée",
            3 : "En location",
            4 : "Location fini",
            5 : "Réfusée",
        }
        return res[self.status]
    def set_bg_color(self):
        res = {
            1 : "#ffb9af",
            2 : "#b3e9b9",
            3 : "#8292bb",
            4 : "#0fdf13",
            5 : "#fff400",
        }
        return res[self.status]
    def get_price_details(self):
        delta = self.end_at - self.start_at
        return f'{delta.days} jours * {self.article.rental_amount} €'
    def notify(self):

        if self.status == 1 or self.status == "1": ## ok En validation
            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que nous avons bien reçu votre demande n° {self.number}.\n\nVeuillez patienter en attendant la confirmation de votre demande. En attendant, n'hésitez pas à visiter notre site pour découvrir nos dernières nouveautés.\n\nPour suivre les changements, veuillez consulter régulièrement cette page:\n\nhttps://drons.fr/rent/id={self.id}/show/\n\n\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)

            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour,\n\nUne nouvelle demande de location a été engestré.\nVoici quelques détails:\n  Numéro de demande : {self.number}.\n Utilisateur : {self.user.get_last_name()} {self.user.get_first_name()}.\nPour voir la demande : https://drons.fr/rent/id={self.id}/show/"
            email_from = settings.EMAIL_HOST_USER
            emails = ["contact@drons.fr"]
            send_mail( subject, message, email_from, emails )
        elif self.status == 2 or self.status == "2": ## ok Validée
            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que votre demande n° {self.number} a été accepté.\n\nVeuillez suivre les indications sur cette page:\n\nhttps://drons.fr/rent/id={self.id}/show/\n\n\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        elif self.status == 3 or self.status == "3":
            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nD'aujourd'hui, votre location {self.number} est commencé. N'oubliez pas la date de fin.\n\nPour toutes informations, veuillez visiter cette page:\n\nhttps://drons.fr/rent/id={self.id}/show/\n\n\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        elif self.status == 4 or self.status == "4": ## ok Location fini
            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nVotre location est terminé. N'oubliez pas de nous laisser votre avis parrapport à notre service.\n\nPour toutes informations, veuillez visiter cette page:\n\nhttps://drons.fr/rent/id={self.id}/show/\n\n\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        else: # Réfusée
            subject = 'DRONES PRO | Location'
            ## ici
            message =f"Bonjour {self.user.last_name} {self.user.first_name},\n\nNous vous informons votre demande de location n'été pas accepté.\n\nVoici la ou les raisons de refus :\n\n    {self.refused_reasion}\nPour suivre les changements, nous vous invitons à consulter cette page\nhttps://drons.fr/rent/id={self.id}/show/\n\nCordialement,\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
    class Meta:
            ordering = ['status', '-created_at']
class Price_reducted(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="Price_reducted")
    old_price = models.FloatField(blank=False, null=False)
    new_price = models.FloatField(blank=False, null=False)
    start_at = models.DateField(blank=True, null=True)
    end_at = models.DateField(blank=True, null=True)
    # slug = models.SlugField(null=True, blank=True, unique=True)
    # def save(self, *args, **kwargs):
    #     slug_str = "%s %s" % (self.article, self.start_at)
    #     self.slug = slugify(slug_str)
    #     super(Price_reducted, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.article}'

    def get_price_frnce_format(self):
        return '{:,.2f}'.format(self.new_price).replace(',', ' ').replace('.', ',') + ' €'

class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="Favorite_user")
    articles = models.ManyToManyField(Article, blank=True)
    # pieces = models.ManyToManyField(Pieces, blank=True)

    def __str__(self):
        return f"{self.user.last_name}'s Favorite"

class Basket(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="Basket_user")

    def __str__(self):
        return f"{self.user.last_name}'s Basket"

class Wish(models.Model):
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, related_name="Wish_in_basket")
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="article_in_wish")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    quantity = models.IntegerField(default=1)
    def __str__(self):
        return f"List with - {self.article}"
    def get_price(self):
        if self.article.reducted:
            new_price = Price_reducted.objects.filter(article = self.article).first().new_price
            return new_price
        else:
            return self.article.price
    def get_total_price(self):
        if self.article.reducted:
            new_price = Price_reducted.objects.filter(article = self.article).first().new_price
            new_price = self.quantity * new_price
            return round(new_price, 2)
        return self.quantity * self.article.price
    def get_price_frnce_format(self):
        return '{:,.2f}'.format(self.price).replace(',', ' ').replace('.', ',') + ' €'
    def get_total_price_frnce_format(self):
        return '{:,.2f}'.format(self.get_total_price()).replace(',', ' ').replace('.', ',') + ' €'

class Repair(models.Model):
    readed = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="Repair_drone_user")
    created = models.DateTimeField(auto_now_add=True, editable=False)
    drone_siret = models.CharField(max_length=255, blank=False, null=False)
    drone_damages = models.CharField(max_length=255, blank=True)
    images =  models.ManyToManyField(Images, blank=True)
    status = models.IntegerField(default=1)
    repair_number = models.CharField(max_length = 10, blank=True, null=True, editable=False, unique=True, default=create_new_ref_number)
    refused_reasion = models.CharField(max_length=255, blank=True, null=True)
    tracking_number = models.CharField(max_length=255, blank=True, null=True)
    amount = models.FloatField(blank=True, null=True)
    first_response_amount = models.CharField(max_length=255, blank=True)
    first_response_duration = models.CharField(max_length=255, blank=True)
    active = models.BooleanField(default=True)
    finished = models.BooleanField(default=False)
    rated = models.BooleanField(default=False)
    notes = models.CharField(max_length=255, null=True, blank=True)
    dovis = models.CharField(max_length=255, null=True, blank=True)
    time_counter = models.CharField(max_length=255, blank=True, null=True)
    start_at = models.DateTimeField(blank=True, null=True)
    end_at = models.DateTimeField(blank=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="repair_company", blank=True, null=True)


    class Meta:
            ordering = ['status', '-readed', '-created']

    def __str__(self):
        return f'{self.drone_siret}'

    def get_franch_phrase(self):
        res = {
            1 : "En validation",
            2 : "Validée",
            3 : "Colis envoyé",
            4 : "Colis reçu",
            5 : "En réparation",
            6 : "Réparé",
            7 : "Montant réglé",
            8 : "Colis renvoyé",
            9 : "Réfusée",
        }
        return res[self.status]
    def get_visibility(self):
        return 'Visible' if self.active else 'Archivée'

    def get_colissimo_link(self):
        return f'https://www.laposte.fr/outils/suivre-vos-envois?code={self.tracking_number }&at_campaign=suivi_colis&at_format=mail_colis'
    def reminde(self):
        subject = 'DRONES PRO | Réparation'
        message =f"Bonjour {self.user.get_last_name()} {self.user.first_name},\n\nVotre demande est terminée.\nN'oubliez pas de noter notre service\nPour le faire, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
        email_from = settings.EMAIL_HOST_USER
        emails = [self.user.email]
        send_mail(subject, message, email_from, emails)
    def notify(self):
        if self.status == 1 or self.status == "1":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que nous avons bien reçu votre demande numéro {self.repair_number}.\n\nVeuillez patienter en attendant la confirmation de votre demande. En attendant, n'hésitez pas à visiter notre site pour découvrir nos dernières nouveautés.\n\nPour suivre les changements, veuillez consulter cette page règlement:\n\nhttps://drons.fr/repair/one/id={self.id}/\n\n\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
            ###################################
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour,\n\nUne nouvelle demande de réparation a été créée avec les détails suivants :\nClient :\n    Nom : {self.user.last_name} {self.user.first_name}\n    Email : {self.user.email}\n    Téléphone : {self.user.phone}\n\nProblème(s) :\n    {self.drone_damages}\n\nPour voir la demande:\n\nhttps://drons.fr/repair/one/id={self.id}/"
            email_from = settings.EMAIL_HOST_USER
            emails = ["contact@drons.fr"]
            send_mail(subject, message, email_from, emails)
        elif self.status == 2 or self.status == "2":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\n\nNous vous informons que votre demande de réparation n° {self.repair_number} a été acceptée.\n\nNotre équipe vous a laissé un message.\nVous pouvez voir le message et passer à l'étape suivante.\n\nPour faire cela, nous vous invitons à consulter cette page\n\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
            ###################################
        elif self.status == 3 or self.status == "3":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que nous avons enregistré le numéro de suivi.\n\n\nNous allons vous informer lorsque nous récupérons votre colis.\nPour suivre votre colis:\n{self.get_colissimo_link()}\nPour suivre les changements, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
            ###################################
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour,\n\nLa réparation numéro : {self.repair_number} a été envoyé.\nVoici le numéro de suivi : {self.tracking_number}\nSuivre le cols : {self.get_colissimo_link()}\n\nPour voir la demande:\n\nhttps://drons.fr/repair/one/id={self.id}/"
            email_from = settings.EMAIL_HOST_USER
            emails = ["contact@drons.fr"]
            send_mail( subject, message, email_from, emails )
        elif self.status == 4 or self.status == "4": ## ok
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que nous avons récupéré votre colis.\n\nVotre article sera réparé dès que possible.\nPour suivre les changements, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        elif self.status == 5 or self.status == "5":
            pass
        elif self.status == 6 or self.status == "6":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que votre article est réparé !.\nPour régler votre montant et voir les changements, veuillez consulter cette page\n\n\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        elif self.status == 7 or self.status == "7":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour,\n\nLa réparation numéro : {self.repair_number} a été réglée.\nLe client {self.user.get_last_name()} {self.user.get_first_name()} a payé {self.amount} € \n\nPour voir la demande:\n\nhttps://drons.fr/repair/one/id={self.id}/"
            email_from = settings.EMAIL_HOST_USER
            emails = ["contact@drons.fr"]
            send_mail(subject, message, email_from, emails)
        elif self.status == 8 or self.status == "8":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.get_first_name()},\n\nNous vous informons que nous avons envoyé votre article.\n\nVoici le numéro de suivi : {self.tracking_number}.\nPour suivre le colis :\n{self.get_colissimo_link()}\nPour suivre les changements, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        elif self.status == 9 or self.status == "9":
            subject = 'DRONES PRO | Réparation'
            ## ici
            message =f"Bonjour {self.user.get_last_name()} {self.user.first_name},\n\nNous vous informons que votre article est non réparable de notre part.\n\nVoici la ou les raisons de refus :\n\n    {self.refused_reasion}\nVotre demande sera supprimer dans les 15 jours à venir\nPour suivre les changements, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user.email]
            send_mail(subject, message, email_from, emails)
        else:
            if self.finished and not self.rated:
                subject = 'DRONES PRO | Réparation'
                message =f"Bonjour {self.user.get_last_name()} {self.user.first_name},\n\nVotre demande est terminée.\nN'oubliez pas de noter notre service\nPour le faire, nous vous invitons à consulter cette page\nhttps://drons.fr/repair/one/id={self.id}/\n\nCordialement,\n\nL'équipe de Drones Pro"
                email_from = settings.EMAIL_HOST_USER
                emails = [self.user.email]
                send_mail(subject, message, email_from, emails)

    def get_reparation_duration(self):
        updated_at = self.updated_at
        now = timezone.now()
        time_difference = now - updated_at
        days = time_difference.days
        hours = time_difference.seconds // 3600
        minutes = (time_difference.seconds // 60) % 60
        if days > 0:
            return f"{days} Jours, {hours:02d} heurs et {minutes:02d} minutes"
        else:
            if hours > 0:
                return f"{hours:02d} heurs et {minutes:02d} minutes"
            else:
                if minutes > 0:
                    return f"{minutes:02d} minutes"

    def is_readed(self):
        return True if self.readed else False
    def get_stage_color_class(self):
        if self.status < 2:
            return "text-danger"
        elif self.status >= 2 and self.status < 5:
            return "text-info"
        elif self.status == 5 or self.status == 6:
            return "text-success"
        else:
            return "color-dark"
    def set_first_repense_sentence(self):
        if self.dovis:
            return f'Bonjour {self.user.get_last_name()} {self.user.get_first_name()}.\nNous avons bien reçu votre demande.\nNotre équipe vous a laissé un message:\n"{self.dovis}"\nLe montant de la réparation sera {self.first_response_amount} € (montant d\'envois inclus).\nEt votre drone sera réparé dans {self.first_response_duration} jours.\nCordialement.\nL\'équipe de DRONES PRO.'
        return f'Bonjour {self.user.get_last_name()} {self.user.get_first_name()}.\nNous avons bien reçu votre demande.\nLe montant de la réparation sera {self.first_response_amount} € (montant d\'envois inclus).\nEt votre drone sera réparé dans {self.first_response_duration} jours.\nCordialement.\nL\'équipe de DRONES PRO.'
    def set_bg_color(self):
        res = {
            1 : "#ffb9af",
            2 : "#b3e9b9",
            3 : "#b3e9b9",
            4 : "#b3e9b9",
            5 : "#a6d5f5",
            6 : "#b0a9f5",
            7 : "#b3e9b9",
            8 : "#b3e9b9",
            9 : "#b3e9b9",
        }
        return res[self.status]

    def start_repair(self):
        self.status = 5
        self.start_at = timezone.now()
        self.save()

    def end_repair(self):
        self.status = 6
        self.end_at = timezone.now()
        self.save()
    def get_repair_delta2(self):
        if self.start_at and self.end_at:
            time_difference = self.end_at - self.start_at
            days = time_difference.days
            hours = time_difference.seconds // 3600
            minutes = (time_difference.seconds // 60) % 60
            if days > 0:
                return f"{days} Jours, {hours:02d} heurs et {minutes:02d} minutes"
            else:
                if hours > 0:
                    return f"{hours:02d} heurs et {minutes:02d} minutes"
                else:
                    if minutes > 0:
                        return f"{minutes:02d} minutes"
        else:
            return "En cours"
    def get_repair_delta(self):
        if self.start_at and not self.end_at:
            # get the current time
            now = timezone.now()
            time_difference = now - self.start_at
            days = time_difference.days
            hours = time_difference.seconds // 3600
            minutes = (time_difference.seconds // 60) % 60
            if days > 0:
                return f"{days} Jours, {hours:02d} heurs et {minutes:02d} minutes et en cours"
            else:
                if hours > 0:
                    return f"{hours:02d} heurs et {minutes:02d} minutes et en cours"
                else:
                    if minutes > 0:
                        return f"{minutes:02d} minutes et en cours"

        elif self.start_at and  self.end_at:
            time_difference = self.end_at - self.start_at
            days = time_difference.days
            hours = time_difference.seconds // 3600
            minutes = (time_difference.seconds // 60) % 60
            if days > 0:
                return f"{days} Jours, {hours:02d} heurs et {minutes:02d} minutes"
            else:
                if hours > 0:
                    return f"{hours:02d} heurs et {minutes:02d} minutes"
                else:
                    if minutes > 0:
                        return f"{minutes:02d} minutes"
        else:
            'En cours'

class Item(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name="item_article")
    quantity = models.IntegerField(default=1)
    def __str__(self):
        return f"{self.quantity} of {self.article}"
class Command(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="command_user")
    items = models.ManyToManyField(Item, blank=True)
    command_number = models.CharField(max_length = 10, blank=True, null=True, editable=False, unique=True, default=create_new_ref_number)
    status = models.IntegerField(default=1)
    further_details = models.TextField(max_length=1024, blank=True, null=True)
    tracking_number = models.CharField(max_length=255, blank=True, null=True)
    total_amount = models.FloatField(blank=True, null=True)
    active = models.BooleanField(default= True)
    def __str__(self):
            return f"{self.command_number}"
    def get_total_price(self):
        total = 0
        for item in self.items.all():
            red = Price_reducted.objects.filter(article = item.article).first()
            if not red:
                total += item.article.price * item.quantity
            else:
                total += red.new_price * item.quantity
        return total

    def get_status(self):
        res = {
            1 : "Commande en validation",
            2 : "Commande validée",
            3 : "Commande réfusé",
            4 : "Commande payée",
            5 : "Commande annulée",
            6 : "Commande envoyée",
            7 : "Commande reçu",
        }
        return res[self.status]
    def notify(self):
        pass
    def get_total_amount(self):
        if self.total_amount:
            return self.total_amount
        else:
            return self.get_total_price()
    def get_tracking_number(self):
        return self.tracking_number if self.tracking_number else 'IND'

class Slider_images(models.Model):
    url = models.ImageField(upload_to='slider_images/')
    def __str__(self):
        return f"{self.url}"




class Problem(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    url = models.URLField(blank=False, null=False)
    title = models.CharField(max_length=150, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=False, null=False)
    resolved = models.BooleanField(default=False)
    user_email = models.EmailField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.url}"

    def set_data_att(self):
        return 1 if not self.resolved else 2

    def notify(self):
        if self.user_email:
            # package sended from the user
            # notify the user and the admin
            subject = 'DRONES PRO | Problème'
            ## ici
            message =f"Bonjour,\n\nNous vous remercions pour votre participation à l'amélioration de la qualité de notre site.\n\nNous avons pris en compte votre participation et le problème sera réglé le plus vite possible.\n\nL'équipe de Drones Pro"
            email_from = settings.EMAIL_HOST_USER
            emails = [self.user_email]
            send_mail(subject, message, email_from, emails)
        ###################################
        subject = 'DRONES PRO | Problème'
            ## ici
        message =f"Bonjour,\n\nUn problème a été signalé sur votre site.\nVoici quelques détails du problème :\n   URL : {self.url}\n   Titre : {self.title}\n   Description : {self.description}\n\nMerci de bien vouloir régler ce problème le plus vite possible."
        email_from = settings.EMAIL_HOST_USER
        emails = ["contact@drons.fr"]
        send_mail( subject, message, email_from, emails )

    class Meta:
            ordering = ['resolved', '-created_at']

class Request_message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # full_name = models.CharField(max_length=255, null=True, blank=True)
    # email = models.CharField(max_length=255, null=True, blank=True)
    # subject = models.CharField(max_length=255, null=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_request_message', null=True, blank=True)
    request = models.TextField(max_length=2024, blank=False, null=False)
    answer = models.TextField(max_length=2024, blank=True, null=True)
    images = models.ManyToManyField(Images, blank=True)
    readed = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    class Meta:
            ordering = ['readed', '-created_at']

    def __str__(self):
        return f'{self.user}'
    def set_data_att(self):
        if self.readed:
            return 1
        return 2

    def notify_on_request(self):
        #notify admin
        subject = 'DRONES PRO | Message'
        ## ici
        message =f"Bonjour,\n\nUn nouveau message a été envoyé dans votre liste.\nVoici quelques détails:\n   message : {self.request}\n\nMerci de bien vouloir reprendre au message le plus vite possible.\n\nPour voir le message:\n  https://drons.fr/message/id={self.id}/show\nPour repondre:\n  https://drons.fr/message/id={self.id}/answer"
        email_from = settings.EMAIL_HOST_USER
        emails = ["contact@drons.fr"]
        send_mail( subject, message, email_from, emails )
        #notify user
         ## ici
        subject = 'DRONES PRO | Message'
        message =f"Bonjour,\n\nNous vous remercions pour votre message.\nNous allons repondre au message dans les plus brefs delais.\n\nCordialement,\nL'équipe de Drones Pro"
        email_from = settings.EMAIL_HOST_USER
        emails = [self.user.email]
        send_mail( subject, message, email_from, emails )
    def notify_on_answer(self):
        pass

class Sell(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    number = models.CharField(max_length = 10, blank=True, null=True, editable=False, unique=True, default=create_new_ref_number)
    user = models.ForeignKey(User, related_name='user_sell', on_delete=models.CASCADE)
    status = models.IntegerField(default=1)
    stat = models.IntegerField(default=1)
    serie = models.CharField(max_length=255, blank=False, null=False)
    active = models.BooleanField(default=True)
    readed = models.BooleanField(default=False)
    compnay = models.ForeignKey(Company, related_name='compnay_sell', on_delete=models.SET_NULL, blank=True, null=True)
    title = models.CharField(max_length=150, blank=False, null=False)
    description = models.TextField(max_length=1024, blank=False, null=False)
    accessories = models.TextField(max_length=1024, blank=False, null=False)
    images = models.ManyToManyField(Images, blank=True)
    # resolution = models.ForeignKey(Resolution, on_delete=models.CASCADE, related_name="resolution_sell")
    first_price = models.FloatField(blank=True, null=True)
    final_price = models.FloatField(blank=True, null=True)
    finished = models.BooleanField(default=False)
    rated = models.BooleanField(default=False)
    tracking_number = models.CharField(max_length=255, blank=True, null=True)
    user_iban = models.CharField(max_length=255, blank=True, null=True)
    refused_reasion = models.CharField(max_length=255, blank=True, null=True)
    class Meta:
            ordering = ['status', '-created_at']


    def __str__(self):
        return f'{self.user.get_last_name()}\'s sell'

    def get_serie(self):
        if len(self.serie) > 20:
            return f'{self.serie[:20]}...'
        return self.serie
    def get_title(self):
        if len(self.title) > 20:
            return f'{self.title[:20]}...'
        return self.title

    def get_status(self):
        res = {
            1 : "En étude", # user do the request
            2 : "Validée", # admin accept the request and propose a price
            3 : "Acceptée", # user accepte the price else goto res[11]
            4 : "Colis envoyé par le clinet", #user send the drone
            5 : "Colis reçu par DP", #admin get the drone
            6 : "Tout est OK",#if it's ok, admin confirm the sell else goto res[7]
            7 : "Un problème",#send text to the user
            8 : "Colis renvoyé par DP",#admin resend the drone
            9 : "Colis reçu par le client",#user get his drone
            10 : "Monant réglé", # if not res[7] means all is OK, admin buy the user
            11 : "Réfusé", # if not res[3]
            12 : "Fermée", # if not res[3]
        }
        return res[self.status]
    def get_stat(self):
        res = {
            1: 'Occasion',
            2: 'État satisfaisant',
            3: 'HS',
        }
        return res[self.stat]
    def set_bg_color(self):
        res = {
            1 : "#ffb9af",
            2 : "#b3e9b9",
            3 : "#8292bb",
            4 : "#0fdf13",
            5 : "#fff400",
            6 : "#fff400",
            7 : "#fff400",
            8 : "#fff400",
            9 : "#fff400",
            10 : "#fff400",
            11 : "#fff400",
            12 : "#fff400",
        }
        return res[self.status]
    def get_title(self):
        if len(self.title) > 30:
            return f'{self.title[:30]}...'
        return self.title
    def get_compnay(self):
        if not self.compnay:
            return 'Autre'
        else:
            return self.compnay.name

    def get_status_color(self):
        if not self.readed:
            return 'text-danger '
        if self.status == 1:
            return 'text-info'
        else:
            return 'text-success '
    def get_visibility(self):
        return 'Visible' if self.active else 'Archivée'

    def notify(self):
        pass



class Notes(models.Model):
    user = models.ForeignKey(User, related_name='user_note', on_delete=models.CASCADE)
    article = models.ForeignKey(Article, related_name='article_note', on_delete=models.CASCADE, blank=True, null=True)
    repair = models.ForeignKey(Repair, related_name='repair_note', on_delete=models.CASCADE, blank=True, null=True)
    command = models.ForeignKey(Command, related_name='command_note', on_delete=models.CASCADE, blank=True, null=True)
    rent = models.ForeignKey(Rent, related_name='rent_note', on_delete=models.CASCADE, blank=True, null=True)
    sell = models.ForeignKey(Sell, related_name='sell_note', on_delete=models.CASCADE, blank=True, null=True)
    stars = models.IntegerField(default=5)
    note = models.TextField(max_length=1024, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.IntegerField(default=1)
    gole = models.IntegerField(default=1)
    class Meta:
            ordering = ['status', '-created_at']
    def __str__(self):
        return f"{self.user}'s note"
    def get_status(self):
        res = {
            1 : "Non validée",
            2 : "Validée",
            3 : "Réfusé",
        }
        return res[self.status]
    def get_gole(self):
        res = {
            1 : "Sur le site",
            2 : "Sur des articles",
            3 : "Sur une réparations",
            4 : "Sur une commande",
            5 : "Sur une location",
            6 : "Sur une reprise",
        }
        return res[self.gole]
    def get_note(self):
        if len(self.note) > 400:
            return f'{self.note[:400]}...'
        return self.note

    def draw_stars(self):
        res = ""
        for i in range(5):
            if i < self.stars:
                res += '<i class="fa-solid fa-star fill-color"></i>'
            else:
                res += '<i class="fa-regular fa-star"></i>'
        return res

    def get_note(self):
        if len(self.note) > 200:
            return f'{self.note[:200]}...'
        else:
            return self.note

    def notify(self):
        subject = 'DRONES PRO | AVIS'
         ## ici
        message =f"Bonjour,\n\nUn nouveau avis a été ajouté dans votre liste.\nVoici quelques détails:\n   Utilisateur :\n{self.user}\n   Commentaire :\n{self.note}\n   Notation :\n{self.stars}/5\n\nMerci de vouloir valider ou supprimer l'avis le plus vite possible.\n\nPour voir le plus de détails:\n  https://drons.fr/notes/gole={self.gole}/list"
        email_from = settings.EMAIL_HOST_USER
        emails = ["contact@drons.fr"]
        send_mail( subject, message, email_from, emails )
        subject = 'DRONES PRO | AVIS'
         ## ici
        message =f"Bonjour,\n\nNous vous remercions pour votre avis.\n\nCordialement,\nL'équipe de Drones Pro"
        email_from = settings.EMAIL_HOST_USER
        emails = [self.user.email]
        send_mail( subject, message, email_from, emails )

class index_slider(models.Model):
    image = models.ForeignKey(Images, on_delete=models.CASCADE, related_name='index_slider_image')
    url = models.URLField(blank=True, null=True)

    def __str__(self):
        return f'{self.image.url}'



class History_repair(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    repair = models.ForeignKey(Repair, related_name='repair_history_repair', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='repair_history_user', on_delete=models.CASCADE)
    last_update = models.DateTimeField(default=timezone.now)
    phrase = models.CharField(max_length=255, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)

    class Meta:
        ordering = ['-created_at']

    def save_phrase(self):
        self.phrase = self.get_phrase()
        self.save()
    def get_phrase(self):
        phrases = {
            1: 'Vous avez effectué une nouvelle demande',
            2: 'Demande validée',
            3: 'Vous avez envoyé le colis',
            4: 'Nous avons reçu le colis',
            5: 'Drone en réparation',
            6: 'Drone réparé',
            7: 'Vous avez payé',
            8: 'Vous avons renvoyé le colis',
            9: 'Votre demande est réfusé',
        }
        return phrases[int(self.status)]


    def __str__(self):
        return f'{self.repair.id}\'s history'

class Mail_receiver(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    email = models.EmailField(unique=True)



    def __str__(self):
        return self.email
class Mail(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    subject = models.CharField(max_length=1024)
    content = models.TextField(max_length=2048)
    sended_from = models.EmailField()
    sender = models.ForeignKey(User, related_name='mail_sender', on_delete=models.CASCADE)
    receivers = models.ManyToManyField(Mail_receiver, related_name='mail_to')


    def __str__(self):
        return self.subject

    def get_subject(self):
        if len(self.subject) > 20:
            return f'{self.subject[:20]}...'
        return self.subject
    def get_receiver(self):
        if self.receivers.count() > 1:
            return f'{self.receivers.first().email}\nEt autres'
        return self.receivers.first().email