from django.contrib import admin
from base.models import \
        User, Company, Article, Favorite, Basket,\
        Resolution, Repair, Price_reducted,\
        Images, Details, Address, Article_type, Wish, Tag,\
        Item, Command, Slider_images, Rent, About_us, Iframe,\
        Problem, Request_message, Section, Notes, Sell, index_slider, \
        History_repair, Mail_receiver, Mail

admin.site.register(User)
admin.site.register(Company)
admin.site.register(Favorite)
admin.site.register(Basket)
admin.site.register(Resolution)
admin.site.register(Repair)
admin.site.register(Images)
admin.site.register(Price_reducted)
admin.site.register(Article)
admin.site.register(Article_type)
admin.site.register(Details)
admin.site.register(Address)
admin.site.register(Wish)
admin.site.register(Tag)
admin.site.register(Item)
admin.site.register(Command)
admin.site.register(Slider_images)
admin.site.register(Rent)
admin.site.register(About_us)
admin.site.register(Iframe)
admin.site.register(Problem)
admin.site.register(Request_message)
admin.site.register(History_repair)
admin.site.register(Section)
admin.site.register(Notes)
admin.site.register(Sell)
admin.site.register(index_slider)
admin.site.register(Mail_receiver)
admin.site.register(Mail)

