from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout
from django.contrib import messages
from django.db.models import Q
from django.contrib.auth.hashers import check_password
from base.forms import  CustomUserCreationForm
from .models import *
from django.template.defaulttags import register
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.conf import settings
from django.core.paginator import Paginator
from urllib.parse import urlencode
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse
from django.utils import timezone
from django.db.models import Count
# from .tasks import task__delete_repair


def init(request):
    tags = Tag.objects.all()
    for tag in tags:
        tag.save()
    return JsonResponse(
        {
            'code' :  1,
            "message" : 'Done',
         }, safe=False)


@register.filter
def get_range(value):
    return range(value)

@register.filter
def from_rent(value):
    return True if 'rentable' in value else False
@register.filter
def set_price_format_to_input(value):
    return str(value).replace(',', '.')

@register.filter
def set_phone_number(phone_number):
    phone_number = phone_number.replace(" ", "")
    formatted_number = ""
    for i, digit in enumerate(phone_number):
        if i % 2 == 0 and i > 0:
            formatted_number += " "
        formatted_number += digit
    return formatted_number
@register.filter
def get_article_new_price(article):
    article = Article.objects.filter(id = article.id).first()
    price_reducted = Price_reducted.objects.filter(article = article).first()
    return '{:,.2f}'.format(price_reducted.new_price).replace(',', ' ').replace('.', ',') + ' €'


def page_404(request):
    return render(request, 'handels/404.html', {})

def page_not_yet(request):
    return render(request, 'handels/not_yet.html', {})


def index(request):
    if request.user.is_superuser:
        return redirect('control_panel')
    else:
        return redirect('index_user')

def index_user(request):
    type_hobby_drones = Article_type.objects.filter(id = 1).first()
    type_pro_drones = Article_type.objects.filter(id = 2).first()
    type_fpv_drones = Article_type.objects.filter(id = 3).first()
    type_drones = Article_type.objects.filter(id = 1).first()
    type_pieces = Article_type.objects.filter(id = 4).first()
    type_cameras = Article_type.objects.filter(id = 5).first()
    type_accessories = Article_type.objects.filter(id = 6).first()
    one_week_ago = datetime.now() - timedelta(days=7)
    notes = Notes.objects.filter(gole = 1, status = 2).all()

    novelty = Article.objects.filter(Q(created_at__gte=one_week_ago) & Q(created_at__lte=datetime.now()) & Q(is_active = True)).all()[:10]
    drones_not_used = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_used=False) & Q(is_active = True))[:10]
    drones_occ = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_active = True) & Q(is_occasion = True))[:10]
    drones_reducted = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(reducted = True) & Q(is_active = True))[:10]
    pieces = Article.objects.filter(article_type = type_pieces, is_active = True).all()[:10]
    cameras = Article.objects.filter(article_type = type_cameras, is_active = True).all()[:10]
    accessories = Article.objects.filter(article_type = type_accessories, is_active = True).all()[:10]


    context = {
        "novelty" : novelty,
        "drones_not_used" : drones_not_used,
        "drones_reducted" : drones_reducted,
        "drones_occ" : drones_occ,
        'pieces' : pieces,
        'cameras' : cameras,
        'accessories' : accessories,
        'notes' : notes,
        }
    return render(request, 'index.html', context)


def auth(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == "POST":
        email = request.POST.get('login-email')
        password = request.POST.get('login-password')
        if not email:
            registe_form = CustomUserCreationForm(request.POST)
            if registe_form.is_valid():
                user = registe_form.save()
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                messages.add_message(request, messages.SUCCESS, f'Bienvenue sur notre site :)')
                return redirect("index")
            messages.add_message(request, messages.SUCCESS, f'Opss!')
            return render(request=request, template_name="auth/signin_signup.html", context={"registe_form": registe_form, 'errer': registe_form.errors})
        else:
            registe_form = CustomUserCreationForm(request.POST)
            user = User.objects.filter(email=email).first()
            if not user:
                messages.add_message(request, messages.SUCCESS, f'Email ou mot de passe est incorrect')
                return render(request=request, template_name="auth/signin_signup.html", context={"registe_form": registe_form})
            if not check_password(password, user.password):
                messages.add_message(request, messages.SUCCESS, f'Email ou mot de passe est incorrect')
                return render(request=request, template_name="auth/signin_signup.html", context={"registe_form": registe_form})
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.add_message(request, messages.SUCCESS, f'Bienvenue sur notre site :)')
            return redirect("index")
    registe_form = CustomUserCreationForm()
    return render(request=request, template_name="auth/signin_signup.html", context={"registe_form": registe_form})

def logout_user(request):
    if not request.user.is_authenticated: messages.add_message(request, messages.SUCCESS, f'Vous devez être connecté pour vous déconnecter :)')
    else:
        logout(request)
        messages.add_message(request, messages.SUCCESS, f'Au revoir! :)')
    return redirect("index")

@login_required(login_url='/auth')
def repair_new(request):
    if request.method == "POST":
        user = request.user
        phone_number = request.POST.get('phone_number')
        rue_number = request.POST.get('rue_number')
        rue_name = request.POST.get('rue_name')
        postal_code = request.POST.get('postal_code')
        city = request.POST.get('city')
        country = request.POST.get('country')
        drone_siret = request.POST.get('drone_siret')
        drone_damages = request.POST.get('drone_damages')
        name = request.POST.get('name')
        company_id = request.POST.get('company')
        compnay = Company.objects.filter(id = company_id).first()
        address = Address.objects.create(
            rue_number = rue_number,
            rue_name = rue_name,
            postal_code = postal_code,
            city = city,
            country = country
        )
        repair = Repair.objects.create(
            user = user,
            drone_siret = drone_siret,
            drone_damages = drone_damages,
            name = name,
            company = compnay,
        )
        user = User.objects.filter(id=user.id).first()
        user.phone = phone_number
        user.address = address
        user.save()
        if request.FILES.get('form-images'):
            for image in request.FILES.getlist('form-images'):
                img = Images.objects.create(url = image)
                repair.images.add(img)
                repair.save()
        # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
        # history.save_phrase()
        repair.notify()
        messages.add_message(request, messages.SUCCESS, f'Votre demande a été prise en compte, veuillez attendre notre confirmation.')
        return redirect('repair', id=repair.id)

    return render(request, 'repair/create.html')


@login_required(login_url='/auth')
def repair(request, id):
    repair = Repair.objects.filter(id=id).first()
    if not repair:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if repair.user != request.user and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    #     historys = History_repair.objects.filter(repair = repair).all()
    # if int(repair.status) > 4:
    #     history1 = History_repair.objects.filter(repair = repair, status = 5).first()
    #     now = timezone.now()
    #     delta = now - history1.last_update
    #     if int(repair.status > 6):
    #         history2 = History_repair.objects.filter(repair = repair, status = 6).first()
    #         dilta = history2.last_update - history1.last_update
    #     repair.delta = delta


    if request.method == "POST":
        new_status = request.POST.get('new-status')
        if new_status == "9":
            repair.refused_reasion = request.POST.get('refused_reasion')
        elif new_status == "6":
            amount = request.POST.get('amount')
            if not amount:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Vous devez remplir toutes les champs.")
                return redirect(request.META.get('HTTP_REFERER'))
            else:
                repair.end_repair()
                repair.amount = amount
        elif new_status == "2":
            first_response_amount = request.POST.get('first_response-amount')
            first_response_duration = request.POST.get('first_response-duration')
            dovis = request.POST.get('dovis')
            print('dovis', dovis)
            if not first_response_amount or not first_response_duration:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Vous devez remplir toutes les champs.")
                return redirect(request.META.get('HTTP_REFERER'))
            else:
                repair.first_response_amount = first_response_amount
                repair.first_response_duration = first_response_duration
                repair.dovis = dovis
        elif new_status == "4":
            repair.notes = request.POST.get('notes')
        elif new_status == "5":
            repair.start_repair()
        elif new_status == "8":
            tracking_number = request.POST.get('tracking_number')
            if not tracking_number:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Vous devez remplir toutes les champs.")
                return redirect(request.META.get('HTTP_REFERER'))
            else:
                repair.tracking_number = tracking_number
        repair.status = new_status
        repair.save()
        # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
        # history.save_phrase()
        repair.notify()
        return redirect('repair', id=repair.id)
    return render(request, 'repair/show.html', {'repair': repair})

@login_required(login_url='/auth')
def repair_tracking_number(request, id):
    repair = Repair.objects.filter(id=id, active = True).first()
    if not repair:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if repair.user != request.user and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if request.method == "POST":
        tracking_number = request.POST.get('tracking_number')
        repair.tracking_number = tracking_number
        repair.status = 3
        repair.save()
        # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
        # history.save_phrase()
        repair.notify()
        return redirect('repair', id=repair.id)
    return redirect('repair', id=repair.id)

@login_required(login_url='/auth')
def delete_repair(request, id):
    repair = Repair.objects.filter(id=id, active = True).first()
    if not repair:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    repair.delete()
    messages.add_message(request, messages.SUCCESS, f"La demande a été supprimée.")
    return redirect('list_repairs')
@login_required(login_url='/auth')
def repair_finished(request, id):
    repair = Repair.objects.filter(id=id).first()
    print(repair.user == request.user)
    # print()
    if not repair:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if repair.user != request.user and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    repair.finished = True
    repair.save()
    # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
    # history.save_phrase()
    repair.reminde()
    # task__send_email(repair.user, schedule=timezone.now() + timedelta(days=3))
    # task__send_email(repair.user, schedule=3)
    return redirect('repair', id=repair.id)

# def auto__delete_repair(request):
#     task__delete_repair(26)
#     return HttpResponse('done')

# @login_required(login_url='/auth')
# def leave_review_for_repair(request, id):
#     repair = Repair.objects.filter(id=id).first()
#     if not repair:
#         messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
#         return redirect('page_404')
#     if repair.user != request.user or not request.user.is_superuser:
#         messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
#         return redirect('page_404')
#     if request.method == "POST":
#         review = request.POST.get('final-note')
#         stars = int(request.POST.get('stars'))
#         if not review or not stars:
#             messages.add_message(request, messages.SUCCESS, f"Opps 😜. Vous devez remplir toutes les champs.")
#             return redirect(request.META.get('HTTP_REFERER'))
#         else:
#             Repair_review.objects.create(
#                 repair = repair,
#                 review = review,
#                 rating = stars,
#                 user = request.user
#             )
#             repair.rated = True
#             repair.save()
#             messages.add_message(request, messages.SUCCESS, f"Merci pour votre avis.")
#             return redirect(request.META.get('HTTP_REFERER'))
#     return redirect('repair', id=repair.id)



@login_required(login_url='/auth')
def archiving_repair(request, id, action):
    if action not in ['True', 'False', True, False]:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    repair = Repair.objects.filter(id=id).first()
    if not repair:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    repair.active = action
    repair.save()
    # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
    # history.save_phrase()
    return redirect('repair', repair.id)

@login_required(login_url='/auth')
def archiving_article(request, id, action):
    if action not in ['True', 'False', True, False]:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    article =  Article.objects.filter(id=id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    article.is_active = action
    article.save()
    messages.add_message(request, messages.SUCCESS, f"Modifications ont été prise en compte.")
    return redirect('one_article', article.id)

def article_similaires(request, id):
    article =  Article.objects.filter(id=id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    articles = get_similar_articles(article)
    nb_objects = articles.count()
    paginator = Paginator(articles, 30)
    page_number = request.GET.get('page')
    articles = paginator.get_page(page_number)
    return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})

@login_required(login_url='/auth')
def archiving_note(request, id, status):
    if status not in [1, 2, 3]:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    note =  Notes.objects.filter(id=id).first()
    if not note:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    note.status = status
    note.save()
    messages.add_message(request, messages.SUCCESS, f"Modifications ont été prise en compte.")
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def archiving_rent(request, id, action):
    print('id', id)
    print('action', action)
    print('action_type', type(action))
    if action not in ['True', 'False', True, False]:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    rent = Rent.objects.filter(id=id).first()
    if not rent:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    rent.active = action
    rent.save()
    messages.add_message(request, messages.SUCCESS, f"Modification ont été prise en compte.")
    return redirect('list_rent')
@login_required(login_url='/auth')
def close_rent(request, id):
    rent = Rent.objects.filter(id=id).first()
    if not rent:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Cette page n'existe pas.")
        return redirect('page_404')
    rent.finished = True
    rent.save()
    messages.add_message(request, messages.SUCCESS, f"Modification ont été prise en compte.")
    return redirect('one_rent', rent.id)

@login_required(login_url='/auth')
def sort_repair_by_date(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        repair_sorted_date = request.POST.get('repair-sorted-date')
        if repair_sorted_date == "1" or repair_sorted_date == 1:
            today = datetime.now() - timedelta(days=1)
            repairs = Repair.objects.filter(active = True, created__gte = today).all()
            return render(request, 'admin/list_repairs.html', context={"repairs": repairs, 'current_data_id':1})
        elif repair_sorted_date == "2" or repair_sorted_date == 2:
            week = datetime.now() - timedelta(days=7)
            repairs = Repair.objects.filter(active = True, created__gte = week).all()
            return render(request, 'admin/list_repairs.html', context={"repairs": repairs, 'current_data_id':2})
        elif repair_sorted_date == "3" or repair_sorted_date == 3:
            month = datetime.now() - timedelta(days=30)
            repairs = Repair.objects.filter(active = True, created__gte = month).all()
            return render(request, 'admin/list_repairs.html', context={"repairs": repairs, 'current_data_id':3})
        elif repair_sorted_date == "4" or repair_sorted_date == 4:
            year = datetime.now() - timedelta(days=365)
            repairs = Repair.objects.filter(active = True, created__gte = year).all()
            return render(request, 'admin/list_repairs.html', context={"repairs": repairs, 'current_data_id':4})
        repairs = Repair.objects.filter(active = True).all()
    return render(request, 'admin/list_repairs.html', context={"repairs": repairs})

@login_required(login_url='/auth')
def list_repairs(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    return render(request, 'admin/list_repairs.html', context={"repairs": Repair.objects.filter(active = True).all()})
@login_required(login_url='/auth')
def list_repairs_all(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    return render(request, 'admin/list_repairs.html', context={"repairs": Repair.objects.all()})

@login_required(login_url='/auth')
def list_my_repairs(request):
    user = request.user
    return render(request, 'repair/my_repairs.html', context={"repairs": Repair.objects.filter(user=user, active = True).all()})




def repair_presentation(request):
    all_notes = Notes.objects.filter(gole = 3, status = 2).all()
    notes = all_notes[:12]
    has_next = False
    # if all_notes.count() > 12:
    #     has_next = True
    # notes.has_next = has_next
    return render(request, 'repair/presentation.html', context={"notes": notes})

@login_required(login_url='/auth')
def repair_delete(request, id):
    repair = Repair.objects.filter(id=id, active = True).first()
    user = request.user
    if repair.user != user:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    repair.delete()
    return redirect('list_my_repairs')

@login_required(login_url='/auth')
def user_profile(request):
    context = {
        'page' : 1,
        'section' : 1
    }
    return render(request, 'user/account/profile.html', context)

@login_required(login_url='/auth')
def edit_profile(request):
    context = {
        'page' : 1,
        'section' : 2
    }
    if request.method == "POST":
        user = request.user
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        phone = request.POST.get('phone')
        address = request.POST.get('address')
        user.first_name = first_name
        user.last_name = last_name
        user.phone = phone
        user.address = address
        user.save()
        messages.add_message(request, messages.SUCCESS, f"Les modifications ont été effectuées avec succès.")
        return redirect('user_profile')
    return render(request, 'user/account/update_profile.html', context)

@login_required(login_url='/auth')
def edit_address(request):
    context = {
        'page' : 1,
        'section' : 3
    }
    if request.method == "POST":
        user = request.user
        rue_number = request.POST.get('rue_number')
        rue_name = request.POST.get('rue_name')
        postal_code = request.POST.get('postal_code')
        city = request.POST.get('city')
        country = request.POST.get('country')
        if user.address == None:
            address = Address.objects.create(
                rue_number = rue_number,
                rue_name = rue_name,
                postal_code = postal_code,
                city = city,
                country = country
            )
            user.address = address
            user.save()
        else:
            address = user.address
            address.rue_number = rue_number
            address.rue_name = rue_name
            address.postal_code = postal_code
            address.city = city
            address.country = country
            address.save()
        messages.add_message(request, messages.SUCCESS, f"Les modifications ont été effectuées avec succès.")
        return redirect('user_profile')
    return render(request, 'user/account/update_address.html', context)


@login_required(login_url='/auth')
def edit_password(request):
    context = {
        'page' : 1,
        'section' : 4
    }
    return render(request, 'user/account/update_password.html', context)


@login_required(login_url='/auth')
def myfavorites(request):
    favorite, created = Favorite.objects.get_or_create(user = request.user)
    is_empty = True
    if len(favorite.articles.all()) != 0 :
        is_empty = False
    context = {
        'page' : 2,
        'section' : 2,
        'favorite' : favorite,
        'is_empty' : is_empty
    }
    return render(request, 'user/list/favorites.html', context)


@login_required(login_url='/auth')
def myrepairs(request):

    context = {
        "repairs": Repair.objects.filter(user=request.user, active = True).all(),
        'page' : 2,
        'section' : 3
    }
    return render(request, 'user/list/myrepairs.html', context)


@login_required(login_url='/auth')
def mysells(request):

    context = {
        "sells": Sell.objects.filter(user=request.user).all(),
        'page' : 2,
        'section' : 3
    }
    return render(request, 'user/list/sells.html', context)



@login_required(login_url='/auth')
def myrented(request):
    rents = Rent.objects.filter(user = request.user, active = True).all()
    context = {
        'page' : 2,
        'section' : 5,
        'rents' : rents,
    }
    return render(request, 'user/list/drone-rented.html', context)



@login_required(login_url='/auth')
def add_article_to_favorites(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        user = request.user
        favorites, created = Favorite.objects.get_or_create(user = user)
        favorites.articles.add(article)
        favorites.save()
        messages.add_message(request, messages.SUCCESS, f"L'article a été ajouté avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def delete_article_from_favorites(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        user = request.user
        favorites, created = Favorite.objects.get_or_create(user = user)
        favorites.articles.remove(article)
        favorites.save()
        messages.add_message(request, messages.SUCCESS, f"L'article a été supprimé avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))




######################################################################################
################################## ADMIN #############################################
######################################################################################
@login_required(login_url='/auth')
def control_panel(request):
    if not request.user.is_superuser:
        return redirect('index')
    return render(request, 'admin/control_panel.html',  context={"repairs": Repair.objects.filter(active = True).all()})


@login_required(login_url='/auth')
def list_users(request):
    if not request.user.is_superuser:
        return redirect('index')
    users = User.objects.all()
    return render(request, 'admin/list_users.html', {'users':users})

@login_required(login_url='/auth')
def delete_one_slider_image(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    slider_image = Slider_images.objects.filter(id = id).first()
    if not slider_image:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        slider_image.delete()
        messages.add_message(request, messages.SUCCESS, f"L'image a été supprimée.")
        return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='/auth')
def edit_company_info(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    about_us = About_us.objects.filter(id = 1).first()
    if not about_us:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        email = request.POST.get('new_email')
        phone = request.POST.get('new_phone')
        address = request.POST.get('new_address')
        google_maps_link = request.POST.get('new_address_url')
        site_title = request.POST.get('new_site_title')
        about_us.email = email
        about_us.phone = phone
        about_us.address = address
        about_us.google_maps_link = google_maps_link
        about_us.site_title = site_title
        about_us.save()
        messages.add_message(request, messages.SUCCESS, f"Les informations ont été modifiées.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, "admin/compnay-info.html", {'about_us' : about_us})

@login_required(login_url='/auth')
def edit_slider_images(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        if request.FILES.get('form-images'):
            for image in request.FILES.getlist('form-images'):
                img = Slider_images.objects.create(url = image)
            messages.add_message(request, messages.SUCCESS, f"Les images ont été enregistrées avec succès.")
            return redirect('index')
    return render(request, "admin/slider.html")

@login_required(login_url='/auth')
def edit_secondary_slider(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        if not request.POST.get('slider-1-url') or not request.FILES.get('slider-1-img') or not request.POST.get('slider-2-url') or not request.FILES.get('slider-2-img'):
            messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
            return redirect(request.META.get('HTTP_REFERER'))
        url_1 = request.POST.get('slider-1-url')
        url_2 = request.POST.get('slider-2-url')
        img1 = Images.objects.create(url = request.FILES.get('slider-1-img'))
        img2 = Images.objects.create(url = request.FILES.get('slider-2-img'))
        index_sliders = index_slider.objects.all()
        for i in index_sliders:
            i.delete()
        index_slider.objects.create(image = img1, url = url_1)
        index_slider.objects.create(image = img2, url = url_2)
        messages.add_message(request, messages.SUCCESS, f"Les images ont été enregistrées avec succès.")
        return redirect('index_user')
    sliders = index_slider.objects.all()
    return render(request, "admin/secondary_slider.html", {"sliders":sliders})
#################################################################################
"""  article  """
#################################################################################

@login_required(login_url='/auth')
def delete_article(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        article = Article.objects.filter(id = id).first()
        if not article:
            messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
            return redirect('page_404')
        else:
            # article.is_active = False
            # article.save()
            article.delete()
            return redirect('index_user')

@login_required(login_url='/auth')
def new_article(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        tags = request.POST.getlist('tags')
        article_type = request.POST.get('article_type')
        title = request.POST.get('title')

        iframe_1_title = request.POST.get('iframe-1-title') if request.POST.get('iframe-1-title') else None
        iframe_1_description = request.POST.get('iframe-1-description') if request.POST.get('iframe-1-description') else None
        iframe_1_url = request.POST.get('iframe-1-link') if request.POST.get('iframe-1-link') else None

        iframe_2_title = request.POST.get('iframe-2-title') if request.POST.get('iframe-2-title') else None
        iframe_2_description = request.POST.get('iframe-2-description') if request.POST.get('iframe-2-description') else None
        iframe_2_url = request.POST.get('iframe-2-link') if request.POST.get('iframe-2-link') else None



        description = request.POST.get('description')
        price = float(request.POST.get('price'))
        compnay_id = request.POST.get('compnay')
        resolution_id = request.POST.get('resolution')
        quantity = request.POST.get('quantity')
        refurbished = request.POST.get('refurbished')
        occasion = request.POST.get('occasion')

        weight = request.POST.get('weight')
        height = request.POST.get('height')
        width = request.POST.get('width')
        color = request.POST.get('color')

        description_2 = request.POST.get('description_2')
        speed = request.POST.get('speed') if request.POST.get('speed') else None
        bat_cap_h = request.POST.get('bat_cap_h') if request.POST.get('bat_cap_h') else None
        flight_time = request.POST.get('flight_time') if request.POST.get('flight_time') else None
        flight_range = request.POST.get('flight_range') if request.POST.get('flight_range') else None
        transmission_range = request.POST.get('transmission_range') if request.POST.get('transmission_range') else None

        if refurbished == "true":
            refurbished = True
        else:
            refurbished = False

        if occasion == "true":
            occasion = True
        else:
            occasion = False
        rentable = request.POST.get('rentable')
        if rentable == "true":
            rentable = True
            rentable_daily_price = float(request.POST.get('rentable-daily-price'))
            rentable_accessories = request.POST.get('rentable_accessories')
        else:
            rentable = False
            rentable_daily_price = None
            rentable_accessories = None


        price_2 = None if len(request.POST.get('price_2')) == 0 else float(request.POST.get('price_2'))
        article_reducted = False
        if price_2 != None:
            article_reducted = True

        company = Company.objects.filter(id=compnay_id).first()
        resolution = Resolution.objects.filter(id=resolution_id).first()
        article_type = Article_type.objects.filter(id=article_type).first()
        article = Article.objects.create(
            added_by = request.user,
            title = title,
            description = description,
            price = price,
            company = company,
            resolution = resolution,
            reducted = article_reducted,
            quantity = quantity,
            is_used = refurbished,
            is_occasion = occasion,
            is_rentable = rentable,
            rental_amount = rentable_daily_price,
            rental_accessories = rentable_accessories,
            article_type = article_type,
            weight = weight,
            height = height,
            width = width,
            color = color,
            speed = speed,
            bat_cap_h = bat_cap_h,
            flight_time = flight_time,
            flight_range = flight_range,
            transmission_range = transmission_range,
            description_2 = description_2,
        )
        article_type.nb_article += 1
        article_type.save()
        for t in tags:
            tag = Tag.objects.filter(id = int(t)).first()
            article.tags.add(tag)
        article.save()
        if request.FILES.get('form-images'):
            for image in request.FILES.getlist('form-images'):
                img = Images.objects.create(url = image)
                article.images.add(img)
                article.save()
        if article_reducted:
            start_at = request.POST.get('start_at')
            end_at = request.POST.get('end_at')
            Price_reducted.objects.create(
                article = article,
                old_price = price,
                new_price = price_2,
                start_at = start_at,
                end_at = end_at,
            )
        inputs_details_counter = int(request.POST.get("inputsDetailsCounter"))
        if inputs_details_counter>0:
            for input in range(inputs_details_counter):
                details_name = request.POST.get(f'details-{input+1}-name')
                details_value = request.POST.get(f'details-{input+1}-value')
                details = Details.objects.create(
                    name = details_name,
                    value = details_value
                )
                article.details.add(details)
                article.save()
            messages.add_message(request, messages.ERROR, f"L'article a été ajouté avec succès.")
            return redirect('one_article', id=article.id)
        if iframe_1_url:
            ifram = Iframe.objects.create(
                title = iframe_1_title,
                description = iframe_1_description,
                url = iframe_1_url
            )
            article.iframe.add(ifram)
            article.save()
        if iframe_2_url:
            ifram = Iframe.objects.create(
                title = iframe_2_title,
                description = iframe_2_description,
                url = iframe_2_url
            )
            article.iframe.add(ifram)
            article.save()

        images_section_1_title = request.POST.get('image-section-1-title') if request.POST.get('image-section-1-title') else None
        images_section_1_description = request.POST.get('image-section-1-description') if request.POST.get('image-section-1-description') else None
        section = Section.objects.create(
                title = images_section_1_title,
                description = images_section_1_description,
            )
        if request.FILES.get('image-section-1-images'):
            section = Section.objects.create(
                title = images_section_1_title,
                description = images_section_1_description,
            )
            for image in request.FILES.getlist('image-section-1-images'):
                img = Images.objects.create(url = image)
                section.image.add(img)
                section.save()
        article.sections.add(section)



        images_section_2_title = request.POST.get('image-section-2-title') if request.POST.get('image-section-2-title') else None
        images_section_2_description = request.POST.get('image-section-2-description') if request.POST.get('image-section-2-description') else None
        section = Section.objects.create(
                title = images_section_2_title,
                description = images_section_2_description,
            )
        if request.FILES.get('image-section-2-images'):
            for image in request.FILES.getlist('image-section-2-images'):
                img = Images.objects.create(url = image)
                section.image.add(img)
                section.save()
        article.sections.add(section)
        if request.FILES.get('main-image-input'):
            file = request.FILES.get('main-image-input')
            img = Images.objects.create(url = file)
            article.main_image = img
        article.save()

        messages.add_message(request, messages.ERROR, f"L'article a été ajouté avec succès.")
        return redirect('one_article', id=article.id)
    return render(request, 'article/create.html')

def one_article(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if not article.is_active and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        if request.user.is_authenticated:
            basket, created = Basket.objects.get_or_create(user = request.user)
            favorite, created = Favorite.objects.get_or_create(user = request.user)
            article.article_in_favorite = True if article in favorite.articles.all() else False
            wish = Wish.objects.filter(basket = basket, article = article).exists()
            article.article_in_basket = True if wish else False
            if article.reducted:
                red = Price_reducted.objects.filter(article = article).first()
                now = datetime.now().date()
                if now > red.end_at:
                    article.reducted = False
                    article.save()
                    red.delete()

        similaires = get_similar_articles(article)
        print(len(similaires))
        sim_has_next = False
        if len(similaires) > 10:
            sim_has_next = True

        return render(request, 'article/show.html', {'article' : article, 'similaires': similaires[:10], 'sim_has_next':sim_has_next})
        # return render(request, 'article/show.html', {'article' : article})

def one_article_rent(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if not article.is_active and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        if request.user.is_authenticated:
            basket, created = Basket.objects.get_or_create(user = request.user)
            favorite, created = Favorite.objects.get_or_create(user = request.user)
            article.article_in_favorite = True if article in favorite.articles.all() else False
            wish = Wish.objects.filter(basket = basket, article = article).exists()
            article.article_in_basket = True if wish else False
            if article.reducted:
                red = Price_reducted.objects.filter(article = article).first()
                now = datetime.now().date()
                if now > red.end_at:
                    article.reducted = False
                    article.save()
                    red.delete()

        similaires = get_similar_articles(article)
        print(len(similaires))
        sim_has_next = False
        if len(similaires) > 10:
            sim_has_next = True

        return render(request, 'article/show-rent.html', {'article' : article, 'similaires': similaires[:10], 'sim_has_next':sim_has_next})

def get_similar_articles(article):
    # Retrieve the values from the given article
    title = article.title
    tags = article.tags.all()
    description = article.description
    article_type = article.article_type

    # Query for similar articles
    similar_articles = Article.objects.filter(
        Q(title__icontains=title) |
        Q(tags__in=tags) |
        Q(description__icontains=description) |
        Q(article_type=article_type)
    ).exclude(id=article.id).distinct()

    return similar_articles


@login_required(login_url='/auth')
def doublicat_article(request, id):
    article = Article.objects.filter(id = id).first()
    if not article or not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        # doublicat the article
        new_article = Article.objects.create(
            title = article.title,
            description = article.description,
            description_2 = article.description_2,
            price = article.price,
            reducted = article.reducted,
            quantity = article.quantity,
            is_used = article.is_used,
            added_by = request.user,
            company = article.company,
            article_type = article.article_type,
            resolution = article.resolution,
            is_occasion = article.is_occasion,
            weight = article.weight,
            height = article.height,
            width = article.width,
            color = article.color,
            speed = article.speed,
            bat_cap_h = article.bat_cap_h,
            flight_time = article.flight_time,
            flight_range = article.flight_range,
            is_rentable = article.is_rentable,
            rental_amount = article.rental_amount,
            rental_accessories = article.rental_accessories,
            transmission_range = article.transmission_range,
        )
        article_type = Article_type.objects.filter(id = article.article_type.id).first()
        article_type.nb_article += 1
        article_type.save()
        if article.main_image:
            new_main_image = Images.objects.create(
                url = article.main_image.url
            )
            new_article.main_image = new_main_image
        for image in article.images.all():
            new_image = Images.objects.create( url = image.url )
            new_article.images.add(new_image)
        for detail in article.details.all():
            new_detail = Details.objects.create(name = detail.name, value = detail.value)
            new_article.details.add(new_detail)
        for tag in article.tags.all():
            new_article.tags.add(tag)
        for iframe in article.iframe.all():
            new_article.iframe.add(iframe)
        for section in article.sections.all():
            new_article.sections.add(section)
        new_article.save()
        red = Price_reducted.objects.filter(article = article).first()
        if red:
            new_red = Price_reducted.objects.create(
                article = new_article,
                old_price = red.old_price,
                new_price = red.new_price,
                start_at = red.start_at,
                end_at = red.end_at
                )
        messages.add_message(request, messages.SUCCESS, f"L'article a été doublée avec succès.")
        return redirect('one_article', new_article.id)

@login_required(login_url='/auth')
def edit_article(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if article.reducted:
        red = Price_reducted.objects.filter(article = article).first()
        article.red_new_price = red.new_price
        article.dated = (red.start_at, red.end_at)
    if request.method == "POST":
        tags = request.POST.getlist('tags')
        article_type = request.POST.get('article_type')
        title = request.POST.get('title')
        description = request.POST.get('description')
        price = float(request.POST.get('price'))
        compnay_id = request.POST.get('compnay')
        resolution_id = request.POST.get('resolution')
        quantity = request.POST.get('quantity')
        refurbished = request.POST.get('refurbished')
        occasion = request.POST.get('occasion')
        weight = request.POST.get('weight') if request.POST.get('weight') else None
        height = request.POST.get('height') if request.POST.get('height') else None
        width = request.POST.get('width') if request.POST.get('width') else None
        color = request.POST.get('color')
        speed = request.POST.get('speed') if request.POST.get('speed') else None
        bat_cap_h = request.POST.get('bat_cap_h') if request.POST.get('bat_cap_h') else None
        flight_time = request.POST.get('flight_time') if request.POST.get('flight_time') else None
        flight_range = request.POST.get('flight_range') if request.POST.get('flight_range') else None
        transmission_range = request.POST.get('transmission_range') if request.POST.get('transmission_range') else None
        description_2 = request.POST.get('description_2') if request.POST.get('description_2') else None

        iframe_1_title = request.POST.get('iframe-1-title') if request.POST.get('iframe-1-title') else None
        iframe_1_description = request.POST.get('iframe-1-description') if request.POST.get('iframe-1-description') else None
        iframe_1_url = request.POST.get('iframe-1-link') if request.POST.get('iframe-1-link') else None

        iframe_2_title = request.POST.get('iframe-2-title') if request.POST.get('iframe-2-title') else None
        iframe_2_description = request.POST.get('iframe-2-description') if request.POST.get('iframe-2-description') else None
        iframe_2_url = request.POST.get('iframe-2-link') if request.POST.get('iframe-2-link') else None


        if refurbished == "true":
            refurbished = True
        else:
            refurbished = False


        if occasion == "true":
            occasion = True
        else:
            occasion = False

        rentable = request.POST.get('rentable')
        if rentable == "true":
            rentable = True
            rentable_daily_price = float(request.POST.get('rentable-daily-price'))
            rentable_accessories = request.POST.get('rentable_accessories')
        else:
            rentable = False
            rentable_daily_price = None
            rentable_accessories = None

        reducted = True if request.POST.get('reducted') == '1' else False
        article.tags.clear()
        for t in tags:
            tag = Tag.objects.filter(id = int(t)).first()
            article.tags.add(tag)
        article.save()
        images_section_1_title = request.POST.get('image-section-1-title') if request.POST.get('image-section-1-title') else None
        images_section_1_description = request.POST.get('image-section-1-description') if request.POST.get('image-section-1-description') else None
        try:
            section = article.sections.all()[0]
        except:
            section = Section.objects.create()
            article.sections.add(section)
            article.save()
        if section:
            section.title = images_section_1_title
            section.description = images_section_1_description
            if request.FILES.get('image-section-1-images'):
                for image in request.FILES.getlist('image-section-1-images'):
                    img = Images.objects.create(url = image)
                    section.image.add(img)
            section.save()
        images_section_2_title = request.POST.get('image-section-2-title') if request.POST.get('image-section-2-title') else None
        images_section_2_description = request.POST.get('image-section-2-description') if request.POST.get('image-section-2-description') else None
        try:
            section = article.sections.all()[1]
        except:
            section = Section.objects.create()
            article.sections.add(section)
            article.save()
        if section:
            section.title = images_section_2_title
            section.description = images_section_2_description
            if request.FILES.get('image-section-2-images'):
                for image in request.FILES.getlist('image-section-2-images'):
                    img = Images.objects.create(url = image)
                    section.image.add(img)
            section.save()


        company = Company.objects.filter(id=compnay_id).first()
        resolution = Resolution.objects.filter(id=resolution_id).first()
        article_type = Article_type.objects.filter(id=int(article_type)).first()
        article.title = title
        article.description = description
        article.price = price
        article.company = company
        article.resolution = resolution
        article.reducted = reducted
        article.quantity = quantity
        article.is_used = refurbished
        article.is_occasion = occasion
        article.is_rentable = rentable
        article.rental_amount = rentable_daily_price
        article.rental_accessories = rentable_accessories
        article.article_type = article_type
        article.weight = weight
        article.height = height
        article.width = width
        article.color = color
        article.speed = speed
        article.bat_cap_h = bat_cap_h
        article.flight_time = flight_time
        article.flight_range = flight_range
        article.transmission_range = transmission_range
        article.description_2 = description_2
        article.save()
        if reducted:
            red = Price_reducted.objects.filter(article = article).first()
            if not red:
                red = Price_reducted.objects.create(
                    article = article,
                    start_at = request.POST.get('start_at'),
                    end_at = request.POST.get('end_at'),
                    old_price = float(request.POST.get('price')),
                    new_price = float(request.POST.get('price_2'))
                )
            else:
                red.start_at = request.POST.get('start_at')
                red.end_at = request.POST.get('end_at')
                red.old_price = float(request.POST.get('price'))
                red.new_price = float(request.POST.get('price_2'))
                red.save()
        else:
            red = Price_reducted.objects.filter(article = article).first()
            if red:
                red.delete()
        if request.FILES.get('form-images'):
            for image in request.FILES.getlist('form-images'):
                img = Images.objects.create(url = image)
                article.images.add(img)

        inputs_details_counter = int(request.POST.get("inputsDetailsCounter"))
        if inputs_details_counter>0:
            for input in range(inputs_details_counter):
                details_name = request.POST.get(f'details-{input+1}-name')
                details_value = request.POST.get(f'details-{input+1}-value')
                details = Details.objects.create(
                    name = details_name,
                    value = details_value
                )
                article.details.add(details)
                article.save()

        for detail in article.details.all():
            old_details_name = request.POST.get(f'old-details-{ detail.id }-name')
            old_details_value = request.POST.get(f'old-details-{ detail.id }-value')
            if old_details_name:
                detail.name = old_details_name
                detail.value = old_details_value
                detail.save()
        article.iframe.clear()
        if iframe_1_url:
            ifram = Iframe.objects.create(
                title = iframe_1_title,
                description = iframe_1_description,
                url = iframe_1_url
            )
            article.iframe.add(ifram)
        if iframe_2_url:
            ifram = Iframe.objects.create(
                title = iframe_2_title,
                description = iframe_2_description,
                url = iframe_2_url
            )
            article.iframe.add(ifram)
        if request.FILES.get('main-image-input'):
            file = request.FILES.get('main-image-input')
            img = Images.objects.create(url = file)
            article.main_image = img

        article.save()
        messages.add_message(request, messages.ERROR, f"L'article a été modifié avec succès.")
        return redirect('one_article', id=article.id)

    return render(request, 'article/edit.html', {'article' : article})

def search_with_query(request):
    q = request.GET.get('q', '')
    if q == '':
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        tags = Tag.objects.filter(name__icontains=q)
        articles = Article.objects.filter(Q(title__icontains=q) | Q(description__icontains=q) | Q(price__icontains=q) | Q(tags__in=tags)).distinct()
        nb_objects = articles.count()
        paginator = Paginator(articles, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)

        # Preserve existing parameters while adding the page parameter
        query_params = request.GET.copy()
        query_params['page'] = page_number
        encoded_params = query_params.urlencode()

        return render(
            request,
            'article/list.html',
            {
                'articles': articles,
                'encoded_params': encoded_params,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées avec la recherche de {q}',
                }
            )


def get_article_by_tag(request, tag):
    tag = Tag.objects.filter(id=tag).first()
    if not tag:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    # articles = Article.objects.filter(tags=tag, is_active = True)
    articles = Article.objects.filter(tags__name__iexact=tag.name, is_active=True)
    nb_objects = articles.count()
    paginator = Paginator(articles, 30)
    page_number = request.GET.get('page')
    articles = paginator.get_page(page_number)
    return render(
        request,
        'article/list.html',
        {
            'articles' : articles,
            'nb_objects':nb_objects,
            'sentence' : f'{nb_objects} résultats trouvées dans {tag.name}',
            }
        )

def get_article_by_compnay(request, id):
    compnay = Company.objects.filter(id=id).first()
    if not compnay:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    articles = Article.objects.filter(company = compnay).all()
    nb_objects = articles.count()
    paginator = Paginator(articles, 30)
    page_number = request.GET.get('page')
    articles = paginator.get_page(page_number)
    return render(
        request,
        'article/list.html',
        {
            'articles' : articles,
            'nb_objects':nb_objects,
            'sentence' : f'{nb_objects} résultats trouvées dans {compnay.name}',
            }
        )


def list_article_with_type(request, object_type):
    if object_type == 0:
        articles = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_active = True)).all()
        nb_objects = articles.count()
        paginator = Paginator(articles, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées',
                }
            )
    article_type = Article_type.objects.filter(id=object_type).first()
    if not article_type:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    articles = Article.objects.filter(article_type = article_type, is_active=True).all()
    nb_objects = articles.count()
    paginator = Paginator(articles, 30)
    page_number = request.GET.get('page')
    articles = paginator.get_page(page_number)
    return render(
        request,
        'article/list.html',
        {
            'articles' : articles,
            'nb_objects':nb_objects,
            'sentence' : f'{nb_objects} résultats trouvées dans {article_type.name}',
            })


def list_article_sorted(request, q):
    type_drones = Article_type.objects.filter(id = 1).first()
    type_pieces = Article_type.objects.filter(id = 2).first()
    type_cameras = Article_type.objects.filter(id = 3).first()
    type_accessories = Article_type.objects.filter(id = 4).first()
    if q == "novelty":
        one_week_ago = datetime.now() - timedelta(days=7)
        novelty = Article.objects.filter(Q(created_at__gte=one_week_ago) & Q(created_at__lte=datetime.now()) & Q(is_active = True)).all()
        nb_objects = novelty.count()
        paginator = Paginator(novelty, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans nos nouveautés',
                }
            )
    elif q == "reducted":
        drones_reducted = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(reducted = True) & Q(is_active = True))
        nb_objects = drones_reducted.count()
        paginator = Paginator(drones_reducted, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans nous offres',
                }
            )
    elif q == "drones_not_used":
        drones_not_used = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_used = False) & Q(is_active = True))
        nb_objects = drones_not_used.count()
        paginator = Paginator(drones_not_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans drones neuf',
                }
            )
    elif q == "drones_used":
        drones_drones_used = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_used = True) & Q(is_active = True))
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans drones reconditionné',
                }
            )
    elif q == "pieces":
        drones_drones_used = Article.objects.filter(article_type = type_pieces, is_active = True).all()
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans piéces détachées',
                }
            )
    elif q == "cameras":
        drones_drones_used = Article.objects.filter(article_type = type_cameras, is_active = True).all()
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans vaméra stabilisée',
                }
            )
    elif q == "accessories":
        drones_drones_used = Article.objects.filter(article_type = type_accessories, is_active = True).all()
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans accessoires',
                }
            )
    elif q == "occasion":
        drones_drones_used = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_occasion = True) & Q(is_active = True)).all()
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats d\'occasion trouvées',
                }
            )
    elif q == "rentable":
        drones_drones_used = Article.objects.filter(Q(article_type_id__in=[1, 2, 3]) & Q(is_rentable = True) & Q(is_active = True)).all()
        nb_objects = drones_drones_used.count()
        paginator = Paginator(drones_drones_used, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        # return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})
        return render(
            request,
            'article/list.html',
            {
                'articles' : articles,
                'nb_objects':nb_objects,
                'sentence' : f'{nb_objects} résultats trouvées dans la section de location',
                'is_page_rentable': True,
                }
            )
    else:
        articles = Article.objects.filter(is_active = True).all()
        nb_objects = articles.count()
        paginator = Paginator(articles, 30)
        page_number = request.GET.get('page')
        articles = paginator.get_page(page_number)
        return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})







#################################################################################
"""  images  """
#################################################################################
@login_required(login_url='/auth')
def delete_image(request, image_id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    image = Images.objects.filter(id = image_id).first()
    if not image:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    image.delete()
    messages.add_message(request, messages.SUCCESS, f"L'image a été supprimée avec succès .")
    return redirect(request.META.get('HTTP_REFERER'))
#################################################################################
"""  details  """
#################################################################################
@login_required(login_url='/auth')
def delete_detail(request, article_id, detail_id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    detail = Details.objects.filter(id = detail_id).first()
    if not detail:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    detail.delete()
    messages.add_message(request, messages.SUCCESS, f"La détail a été supprimée avec succès .")
    return redirect(request.META.get('HTTP_REFERER'))



@login_required(login_url='/auth')
def new_problem(request):
    if request.method == "POST":
        url = request.POST.get('url')
        title = request.POST.get('title')
        description = request.POST.get('url')
        user_email = request.POST.get('user_email') if request.POST.get('user_email') else None
        problem = Problem.objects.create(
            url = url,
            title = title,
            description = description,
            user_email = user_email)
        problem.notify()
        messages.add_message(request, messages.SUCCESS, f"Le problème a été signalée, merci pour votre participation.")
        return redirect('index_user')
    return render(request, 'problem/create.html')

@login_required(login_url='/auth')
def one_problem(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    problem = Problem.objects.filter(id = id).first()
    if not problem:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    return render(request, 'problem/show.html', {'problem': problem})

@login_required(login_url='/auth')
def solve_problem(request, id, action):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    problem = Problem.objects.filter(id = id).first()
    if not problem:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if action not in ['True', 'False']:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    problem.resolved = action
    problem.active = False
    problem.save()
    messages.add_message(request, messages.SUCCESS, f"Le problème a été résolu avec succès.")
    return redirect('list_problems')

@login_required(login_url='/auth')
def list_problems(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    problems = Problem.objects.all()
    return render(request, 'admin/list_problems.html', {'problems': problems})



@login_required(login_url='/auth')
def new_message(request):
    if request.method == "POST":
        request_msg = request.POST.get('message')
        request_message = Request_message.objects.create(
            request = request_msg,
            user = request.user
        )
        if request.FILES.get('form-images'):
            for image in request.FILES.getlist('form-images'):
                img = Images.objects.create(url = image)
                request_message.images.add(img)
        request_message.save()
        request_message.notify_on_request()
        messages.add_message(request, messages.SUCCESS, f"Merci pour votre message, notre équipe vous répondra le plus vite possible.")
        return redirect('index_user')
    else:
        return render(request, 'message/create.html')

@login_required(login_url='/auth')
def one_message(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    msg = Request_message.objects.filter(id=id).first()
    if not msg:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    msg.readed = True
    msg.save()
    return render(request, 'message/show.html', {'msg': msg})

@login_required(login_url='/auth')
def answer_message(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    msg = Request_message.objects.filter(id=id).first()
    if not msg:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    return render(request, 'message/answer.html', {'msg' : msg})


@login_required(login_url='/auth')
def list_messages(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    list_messages = Request_message.objects.filter(active = True).all()
    return render(request, 'admin/list_messages.html', {'list_messages' : list_messages})
@login_required(login_url='/auth')
def list_archived_messages(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    list_messages = Request_message.objects.filter(active = False).all()
    return render(request, 'admin/list_messages.html', {'list_messages' : list_messages})
@login_required(login_url='/auth')
def list_messages_all(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    list_messages = Request_message.objects.all()
    return render(request, 'admin/list_messages.html', {'list_messages' : list_messages})

@login_required(login_url='/auth')
def sell_list_all(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    sells = Sell.objects.all()
    return render(request, 'admin/list_sell.html', {'sells' : sells})


@login_required(login_url='/auth')
def archiving_message(request, id, action):
    print('action', action)
    if action not in ['True', 'False', True, False]:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    msg = Request_message.objects.filter(id=id).first()
    if not msg:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    msg.active = action
    print('action', action)
    msg.save()
    messages.add_message(request, messages.SUCCESS, f"Modifications ont été prise en compte.")

    return redirect('list_messages')



@login_required(login_url='/auth')
def send_mail_to_client(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == "POST":
        email = request.POST.get('email')
        return render(request, 'admin/new_mail.html', {'email':email})
    return redirect(request.META.get('HTTP_REFERER'))






@login_required(login_url='/auth')
def list_compnay(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    companies = Company.objects.all()
    return render(request, 'admin/list_other.html', {'object_list_id' : 1, 'object_type': 'Entreprises','objects':companies})


@login_required(login_url='/auth')
def list_artiles_type(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    types = Article_type.objects.all()
    return render(request, 'admin/list_other.html', {'object_list_id' : 2, 'object_type': 'Types des articles','objects':types})

@login_required(login_url='/auth')
def list_tags(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    tags = Tag.objects.all()
    return render(request, 'admin/list_other.html', {'object_list_id' : 3, 'object_type': 'Tags','objects':tags})

@login_required(login_url='/auth')
def list_other(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    # tags = Tag.objects.all()
    # types = Article_type.objects.all()
    companies = Company.objects.all()
    tags = Tag.objects.annotate(article_count = Count('article'))
    types = Article_type.objects.all()
    # types = Article_type.objects.annotate(article_count = Count('article'))
    # companies = Company.objects.annotate(article_count = Count('article'))
    return render(request, 'admin/list_other.html', {'tags':tags, 'types':types, 'companies':companies})




@login_required(login_url='/auth')
def user_change_role(request, new_role_id, user_id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    user = User.objects.filter(id = user_id).first()
    if not user:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if new_role_id == 1:
        user.is_staff = True
        user.is_admin = True
        user.is_superuser = True
        user.save()
        messages.add_message(request, messages.SUCCESS, f"l'utilisateur a été promu avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    else:
        user.is_staff = False
        user.is_admin = False
        user.is_superuser = False
        user.save()
        messages.add_message(request, messages.SUCCESS, f"l'utilisateur a été rétrogradé avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))


#################################################################################
"""  basket  """
#################################################################################
@login_required(login_url='/auth')
def mybasket(request):
    basket, created = Basket.objects.get_or_create(user = request.user)
    wishes = Wish.objects.filter(basket = basket).all()

    is_empty = True
    if wishes.count() != 0:
        is_empty = False
    context = {
        'page' : 2,
        'section' : 1,
        'wishes' : wishes,
        'is_empty' : is_empty
    }
    return render(request, 'user/list/basket.html', context)

@login_required(login_url='/auth')
def add_article_to_basket(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        user = request.user
        basket, created = Basket.objects.get_or_create(user = user)
        wish = Wish.objects.create(basket = basket, article = article)
        messages.add_message(request, messages.SUCCESS, f"L'article a été ajouté avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def delete_article_from_basket(request, id):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        user = request.user
        basket, created = Basket.objects.get_or_create(user = user)
        wish = Wish.objects.filter(basket = basket, article = article).first()
        wish.delete()
        messages.add_message(request, messages.SUCCESS, f"L'article a été supprimé avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def set_quantity_in_basket(request, id, action):
    article = Article.objects.filter(id = id).first()
    if not article:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    else:
        user = request.user
        basket, created = Basket.objects.get_or_create(user = user)
        wish = Wish.objects.filter(basket = basket, article = article).first()
        if action == "up":
            wish.quantity += 1
            wish.save()
        else: #action == down
            if wish.quantity > 1:
                wish.quantity -= 1
                wish.save()
            else:
                wish.delete()
        messages.add_message(request, messages.SUCCESS, f"Modifiations ont été fait avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))



#################################################################################
"""  PDF  """
#################################################################################

from io import BytesIO
from django.template.loader import get_template
from django.http import HttpResponse
from xhtml2pdf import pisa
def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None

def get_pdf_test(request):
    return render(request, 'handels/test.html')

def get_pdf_2(request):
    data = {
        "name": "Mama", #you can feach the data from database
        "id": 18,
        "amount": 333,
        }
    pdf = render_to_pdf('handels/test.html',data)
    if pdf:
        response=HttpResponse(pdf,content_type='application/pdf')
        filename = "Report_for_%s.pdf" %(data['id'])
        content = "inline; filename= %s" %(filename)
        response['Content-Disposition']=content
        return response
    return HttpResponse("Page Not Found")


def get_pdf(request):
    data = {
        "name": "Mama",
        "id": 18,
        "amount": 333,
        }
    template_path = 'handels/test.html'
    context = data
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="report.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)

    # create a pdf
    pisa_status = pisa.CreatePDF(
       html, dest=response)
    # if error then show some funny view
    if pisa_status.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response

#################################################################################
"""  MAIL  """
#################################################################################

def mail_sender(request):
    subject = 'Testing Cpanel mail'
    message = "Hello, this is juste a test"
    email_from = settings.EMAIL_HOST_USER
    emails = [
        'mooqeemgh@gmail.com',
        'redwan.oma20@gmail.com'
    ]
    send_mail(
        subject,
        message,
        email_from,
        emails
    )
    return HttpResponse("Email sended")


@login_required(login_url='/auth')
def new_mail(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))

    if request.method == "POST":
        html_counter = int(request.POST.get('email-counter'))
        emails = list()
        for i in range(html_counter):
            emails.append(request.POST.get(f'receiver-{i+1}'))
        subject = request.POST.get('object')
        content = request.POST.get('content')
        email_from = settings.EMAIL_HOST_USER
        send_mail(
            subject,
            content,
            email_from,
            emails
        )
        mail = Mail.objects.create(
            subject = subject,
            content = content,
            sended_from = email_from,
            sender = request.user
        )
        for email in emails:
            receiver, created = Mail_receiver.objects.get_or_create(email = email)
            mail.receivers.add(receiver)
        mail.save()
        messages.add_message(request, messages.SUCCESS, f"Votre mail a été envoyer.")
        return redirect('control_panel')
    return render(request, 'admin/new_mail.html')


@login_required(login_url='/auth')
def one_mail(request, id):
    mail = Mail.objects.filter(id=id).first()
    if not request.user.is_superuser or not mail:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/show_mail.html', {'mail':mail})


def list_mails(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    mails = Mail.objects.all()
    return render(request, 'admin/list_mails.html', {'mails':mails})


#################################################################################
"""  Command  """
#################################################################################

@login_required(login_url='/auth')
def myorders(request):
    commands = Command.objects.filter(user = request.user).all()
    context = {
        'page' : 2,
        'section' : 4,
        'commands' : commands,
    }
    return render(request, 'user/list/myorders.html', context)

@login_required(login_url='/auth')
def all_commands(request):
    if not request.user.is_superuser:
        return redirect('index')
    commands = Command.objects.all()
    return render(request, 'admin/list_commands.html', {'commands':commands})


@login_required(login_url='/auth')
def new_command(request):
    return render(request, 'command/create.html', {})


@login_required(login_url='/auth')
def cancel_command(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    command = Command.objects.filter(id = id).first()
    if not command:
        messages.add_message(request, messages.SUCCESS, f"La commande n'existe pas !.")
        return redirect(request.META.get('HTTP_REFERER'))
    messages.add_message(request, messages.SUCCESS, f"La commande a été annulée avec succès.")
    return redirect(request.META.get('HTTP_REFERER'))


@login_required(login_url='/auth')
def one_command(request, id):
    command = Command.objects.filter(id = id).first()
    if not command:
        messages.add_message(request, messages.SUCCESS, f"La commande n'existe pas !.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'command/show.html', {'command' : command})

@login_required(login_url='/auth')
def edit_command(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    command = Command.objects.filter(id = id).first()
    if not command:
        messages.add_message(request, messages.SUCCESS, f"La commande n'existe pas !.")
        return redirect(request.META.get('HTTP_REFERER'))

    if request.method == "POST":
        new_status = request.POST.get('new_status')
        command.status = new_status
        command.save()
        messages.add_message(request, messages.SUCCESS, f"La commande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    messages.add_message(request, messages.SUCCESS, f"La commande a été annulée avec succès.")
    return redirect(request.META.get('HTTP_REFERER'))









#################################################################################
"""  RENT  """
#################################################################################

def rent_presentation(request):
    all_notes = Notes.objects.filter(gole = 5, status = 2).all()
    notes = all_notes[:6]
    has_next = False
    if all_notes.count() > 6:
        has_next = True
    notes.has_next = has_next
    return render(request, 'rent/presentation.html', context={"notes": notes})

# @login_required(login_url='/auth')
# def new_rent(request, id):
#     article = Article.objects.filter(id = id).first()
#     if not article or not article.is_rentable:
#         messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
#         return redirect(request.META.get('HTTP_REFERER'))
#     rent = Rent.objects.filter(article = article).first()
#     if rent and rent.status >= 2:
#         messages.add_message(request, messages.SUCCESS, f"Opps 😜. Ce drone est déjà loué, essayez un autre.")
#         return redirect(request.META.get('HTTP_REFERER'))
#     rent = Rent.objects.create(
#         user = request.user,
#         article = article
#     )
#     messages.add_message(request, messages.SUCCESS, f"La demande a été faite avec succès.")
#     return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def list_rent(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Ce drone est déjà loué, essayez un autre.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_rent.html', {"rents" : Rent.objects.all()})


@login_required(login_url='/auth')
def new_rent(request, id):
    article = Article.objects.filter(id = id).first()
    if not article or not article.is_rentable:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Ce drone est déjà loué, essayez un autre.")
        return redirect('page_404')
    if request.method == "POST":
        start_at = request.POST.get('start_at')
        end_at = request.POST.get('end_at')
        explanation = request.POST.get('explanation')
        phone_number = request.POST.get('phone_number')
        rue_number = request.POST.get('rue_number')
        rue_name = request.POST.get('rue_name')
        postal_code = request.POST.get('postal_code')
        city = request.POST.get('city')
        country = request.POST.get('country')
        rdv_at = request.POST.get('rdv_at')

        user = request.user
        if not user.address:
            address = Address.objects.create(
                rue_number = rue_number,
                rue_name = rue_name,
                postal_code = postal_code,
                city = city,
                country = country
            )
            user.address = address
            user.phone = phone_number
            user.save()
        rent = Rent.objects.create(
            user = user,
            article = article,
            start_at = start_at,
            end_at = end_at,
            explanation = explanation,
            appointment_at = rdv_at
        )
        rent.notify()
        messages.add_message(request, messages.SUCCESS, f"Votre demande a été faite avec succès.")
        return redirect('myrented')
    return render(request, 'rent/create.html', {'article': article})


@login_required(login_url='/auth')
def one_rent(request, id):
    rent = Rent.objects.filter(id = id).first()
    if not rent:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if rent.user != request.user and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if not rent.active and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == "POST":
        new_status = request.POST.get('new-status')
        print('new_status',new_status)
        rent.status = int(new_status)
        rent.refused_reasion = request.POST.get('refused-reasion')
        rent.save()
        rent.notify()
        messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'rent/show.html', {'rent': rent})





#################################################################################
"""  general  """
#################################################################################

def about_us(request):
    return render(request, 'general/about_us.html')
def privacy(request):
    return render(request, 'general/privacy.html')
def legal_notice(request):
    return render(request, 'general/legal_notice.html')
def contact_us(request):
    return render(request, 'general/contact_us.html')















@login_required(login_url='/auth')
def articles_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    articles = Article.objects.filter(is_active = False).all()
    nb_objects = articles.count()
    return render(request, 'article/list.html', {'articles' : articles, 'nb_objects':nb_objects})



@login_required(login_url='/auth')
def repair_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_repairs.html', {'repairs': Repair.objects.filter(active = False).all()})



@login_required(login_url='/auth')
def orders_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_commands.html', {'articles': Command.objects.filter(active = False).all()})



@login_required(login_url='/auth')
def rents_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_rent.html', {'rents': Rent.objects.filter(active = False).all()})



@login_required(login_url='/auth')
def mails_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'article/list.html', {'articles': Rent.objects.filter(active = False).all()})

@login_required(login_url='/auth')
def notes_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_notes.html', {'notes': Notes.objects.filter(status = 3).all()})


@login_required(login_url='/auth')
def problems_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_problems.html', {'problems': Problem.objects.filter(active = False).all()})


@login_required(login_url='/auth')
def messages_archived(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'admin/list_messages.html', {'list_messages': Request_message.objects.filter(active = False).all()})


def edit_tag(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    tag = Tag.objects.filter(id=id).first()
    if not tag:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == 'POST':
        name = request.POST.get('new_name')
        tag.name = name
        tag.save()
        messages.add_message(request, messages.SUCCESS, f"Le tag a été modifié avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))

def edit_type(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    type = Article_type.objects.filter(id=id).first()
    if not type:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == 'POST':
        name = request.POST.get('new_name')
        type.name = name
        type.save()
        messages.add_message(request, messages.SUCCESS, f"Le type a été modifié avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))

def edit_company(request, id):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    compnay = Company.objects.filter(id=id).first()
    if not compnay:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == 'POST':
        name = request.POST.get('new_name')
        url = request.POST.get('site_url')
        compnay.name = name
        compnay.site_url = url
        compnay.save()
        messages.add_message(request, messages.SUCCESS, f"L'entreprise a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))




#################################################################################
"""  NOTES  """
#################################################################################

@login_required(login_url='/auth')
def list_notes(request, gole):
    if gole not in [1, 2, 3, 4, 5] or not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    notes = Notes.objects.filter(gole = gole, status = 1).all()
    return render(request, 'admin/list_notes.html', {'notes': notes})

@login_required(login_url='/auth')
def edit_note(request, id, status):
    note = Notes.objects.filter(id = id).first()
    if not request.user.is_superuser or not note or status not in [1, 2, 3]:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    note.status = status
    note.save()
    messages.add_message(request, messages.SUCCESS, f"Les modifications ont été prise en compte.")
    return redirect(request.META.get('HTTP_REFERER'))
@login_required(login_url='/auth')
def delete_note(request, id):
    note = Notes.objects.filter(id = id).first()
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    note.delete()
    messages.add_message(request, messages.SUCCESS, f"Les modifications ont été prise en compte.")
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def new_note(request, gole, id):
    if gole not in [1, 2, 3, 4, 5, 6]:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == 'POST':
        stars = request.POST.get('stars')
        final_note = request.POST.get('final-note')
        user = request.user
        note = Notes.objects.create(
            user = user,
            stars = stars,
            note = final_note,
            gole = gole
        )
        if gole == 1:
            pass
        elif gole == 2:
            article = Article.objects.filter(id = id).filter()
            if not article:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect('page_404')
            note.article = article
            note.save()

        elif gole == 3:
            repair = Repair.objects.filter(id = id).first()
            if not repair:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect('page_404')
            note.repair = repair
            repair.rated = True
            repair.finished = True
            repair.save()
            # history = History_repair.objects.create(repair = repair, user = repair.user, status = repair.status)
            # history.save_phrase()
            note.save()
        elif gole == 4:
            command = Command.objects.filter(id = id).first()
            if not command:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect('page_404')
            note.command = command
            note.save()
        elif gole == 5:
            rent = Rent.objects.filter(id = id).first()
            if not rent:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect('page_404')
            note.rent = rent
            rent.finished = True
            rent.rated = True
            rent.save()
            note.save()
        elif gole == 6:
            sell = Sell.objects.filter(id = id).first()
            if not sell:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect('page_404')
            note.sell = sell
            sell.finished = True
            sell.rated = True
            sell.status = 12
            sell.save()
            note.save()
    note.notify()
    messages.add_message(request, messages.SUCCESS, f"Merci pour votre avis.")
    return redirect(request.META.get('HTTP_REFERER'))




@login_required(login_url='/auth')
def admin_leave_review(request):
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect('page_404')
    if request.method == 'POST':
        user = request.POST.get('user_id')
        stars = request.POST.get('stars')
        final_note = request.POST.get('final-note')
        note = Notes.objects.create(
            user = User.objects.filter(id = user).first(),
            stars = stars,
            note = final_note,
            gole = 1,
            status = 2
        )
        return redirect(request.META.get('HTTP_REFERER'))
    return redirect(request.META.get('HTTP_REFERER'))





def all_notes(request, gole):
    if gole not in [1, 2, 3, 4, 5]:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    notes = Notes.objects.filter(status = 2, gole = gole)
    return render(request, 'note/list.html', {'notes': notes})


import io
from django.http import FileResponse
from reportlab.pdfgen import canvas


def some_view(request):
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename="hello.pdf")


@login_required(login_url='/auth')
def one_sell(request, id):
    sell = Sell.objects.filter(id = id).first()
    if not sell:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if sell.user != request.user and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if not sell.active and not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == "POST":
        new_status = int(request.POST.get('new-status'))
        sell.status = new_status
        if new_status == "2" or new_status == 2: # validé
            first_price = request.POST.get('first_price')
            if not first_price:
                messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
                return redirect(request.META.get('HTTP_REFERER'))
            else:
                sell.first_price = float(request.POST.get('first_price'))
        elif new_status == 11:
            refused_reasion = request.POST.get('refused_reasion')
            sell.refused_reasion = refused_reasion
        sell.save()
        sell.notify()
        messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    return render(request, 'sell/show.html', {'sell': sell})

@login_required(login_url='/auth')
def edit_sell(request, number):
    sell = Sell.objects.filter(number = number).first()
    if not sell:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if request.method == "POST":
        new_status = int(request.POST.get('new-status'))
        if new_status == 1 or new_status == '1':
            sell.status = 3
        else:
            sell.status = 12
        sell.save()
        sell.notify()
        messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def delete_sell(request, number):
    sell = Sell.objects.filter(number = number).first()
    if not sell:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    if not request.user.is_superuser:
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    sell.delete()
    messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
    return redirect('sell_list_all')


@login_required(login_url='/auth')
def add_tracking_number_to_sell(request, number):
    sell = Sell.objects.filter(number = number).first()
    if not sell:
        print('here 1')
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    # if not request.user.is_superuser or sell.user != request.user:
    #     print('here 2')
    #     messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    #     return redirect(request.META.get('HTTP_REFERER'))
    if request.method == "POST":
        tracking_number = request.POST.get('tracking_number')
        sell.tracking_number = tracking_number
        sell.status = 4
        sell.save()
        sell.notify()
        messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def finish_sell(request, number):
    sell = Sell.objects.filter(number = number).first()
    if not sell:
        print('here 1')
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    # if not request.user.is_superuser or sell.user != request.user:
    #     print('here 2')
    #     messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    #     return redirect(request.META.get('HTTP_REFERER'))

    sell.status = 10
    sell.finished = True
    sell.save()
    sell.notify()
    messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
    return redirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/auth')
def set_sell_iban(request, number):
    sell = Sell.objects.filter(number = number).first()
    if not sell:
        print('here 1')
        messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
        return redirect(request.META.get('HTTP_REFERER'))
    # if not request.user.is_superuser or sell.user != request.user:
    #     print('here 2')
    #     messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    #     return redirect(request.META.get('HTTP_REFERER'))
    if request.method == "POST":
        iban = request.POST.get('iban')
        sell.user_iban = iban
        sell.save()
        messages.add_message(request, messages.SUCCESS, f"La demande a été modifiée avec succès.")
        return redirect(request.META.get('HTTP_REFERER'))
    messages.add_message(request, messages.SUCCESS, f"Opps 😜. Une erreur s'est produite. Veuillez réessayer plus tard.")
    return redirect(request.META.get('HTTP_REFERER'))



@login_required(login_url='/auth')
def new_sell(request):
    user = request.user
    if request.method == "POST":
        drone_stat = int(request.POST.get('drone-stat'))
        drone_company = request.POST.get('drone-company')
        drone_serie = request.POST.get('drone-serie')
        drone_name = request.POST.get('drone-name')
        drone_accessories = request.POST.get('drone-accessories')
        drone_description = request.POST.get('drone-description')
        drone_images = request.FILES.get('drone-images')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')

        rue_number = request.POST.get('rue_number')
        rue_name = request.POST.get('rue_name')
        postal_code = request.POST.get('postal_code')
        city = request.POST.get('city')
        country = request.POST.get('country')

        user_address = user.address
        if not user_address:
            user_address = Address.objects.create()
        user_address.rue_number = rue_number
        user_address.rue_name = rue_name
        user_address.postal_code = postal_code
        user_address.city = city
        user_address.country = country
        user_address.save()
        compnay = Company.objects.filter(id = drone_company).first()
        sell = Sell.objects.create(
            user = user,
            stat = drone_stat,
            compnay = compnay,
            title = drone_name,
            serie = drone_serie,
            description = drone_description,
            accessories = drone_accessories,
        )
        print('image-input', request.FILES.get('image-input'))
        if request.FILES.get('image-input'):
            for image in request.FILES.getlist('image-input'):
                print('image', image)
                img = Images.objects.create(url = image)
                sell.images.add(img)
        sell.save()
        sell.notify()
        messages.add_message(request, messages.SUCCESS, f"La demande a été faite avec succès.")
        return redirect('mysells')
    return render(request, 'sell/create.html')



def sell_presentation(request):
    all_notes = Notes.objects.filter(gole = 6, status = 2).all()
    notes = all_notes[:6]
    has_next = False
    if all_notes.count() > 6:
        has_next = True
    notes.has_next = has_next
    return render(request, 'sell/presentation.html', {'notes': notes})