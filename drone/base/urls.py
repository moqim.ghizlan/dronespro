from django.urls import path
from . import views

dronsAdmin_urls = [
    path('control-panel', views.control_panel, name='control_panel'),
    path('users/list', views.list_users, name='list_users'),
    path('commands/list', views.all_commands, name='all_commands'),
    path('user/id=<int:user_id>/promotion/to=<int:new_role_id>', views.user_change_role, name='user_change_role'),
    path('mail_sender', views.mail_sender, name='mail_sender'),
    path('new_mail', views.new_mail, name='new_mail'),
    path('mail/one/<int:id>/show', views.one_mail, name='one_mail'),
    path('mail-sended/list', views.list_mails, name='list_mails'),
    path('slider/images/update', views.edit_slider_images, name='edit_slider_images'),
    path('secondary-slider/update', views.edit_secondary_slider, name='edit_secondary_slider'),
    path('slider/images/id=<int:id>/delete', views.delete_one_slider_image, name='delete_one_slider_image'),
    path('company/info/edit', views.edit_company_info, name='edit_company_info'),
    path('problems/list', views.list_problems, name='list_problems'),
    path('message/list', views.list_messages, name='list_messages'),
    path('message/list/archived', views.list_archived_messages, name='list_archived_messages'),
    path('message/list/all', views.list_messages_all, name='list_messages_all'),
    path('sell/list/all', views.sell_list_all, name='sell_list_all'),
    path('client/send-mail', views.send_mail_to_client, name='send_mail_to_client'),
    path('compnay/list', views.list_compnay, name='list_compnay'),
    path('article-type/list', views.list_artiles_type, name='list_artiles_type'),
    path('tags/list', views.list_tags, name='list_tags'),
    path('other/list', views.list_other, name='list_other'),
    path('tag/id=<int:id>/edit', views.edit_tag, name='edit_tag'),
    path('type/id=<int:id>/edit', views.edit_type, name='edit_type'),
    path('company/id=<int:id>/edit', views.edit_company, name='edit_company'),
    path('some_view', views.some_view, name='some_view'),



    path('problem/new', views.new_problem, name='new_problem'),
    path('problem/id=<int:id>/show', views.one_problem, name='one_problem'),
    path('problem/id=<int:id>/<str:action>', views.solve_problem, name='solve_problem'),

    path('message-new', views.new_message, name='new_message'),
    path('message/id=<int:id>/show', views.one_message, name='one_message'),
    path('message/id=<int:id>/answer', views.answer_message, name='answer_message'),
    path('message/id=<int:id>/archiving/set-visible/<str:action>', views.archiving_message, name='archiving_message'),



    path('notes/gole=<int:gole>/list', views.list_notes, name='list_notes'),
    path('notes/id=<int:id>/edit/tobe=<int:status>', views.edit_note, name='edit_note'),
    path('notes/id=<int:id>/delete', views.delete_note, name='delete_note'),
    path('note/one/id=<int:id>/archiving/set-visible/<int:status>', views.archiving_note, name='archiving_note'),
    path('notes/gole=<int:gole>/object=<int:id>create/', views.new_note, name='new_note'),
    path('note/new', views.admin_leave_review, name='admin_leave_review'),
    path('notes/with-gole/gole=<int:gole>/all', views.all_notes, name='all_notes'),
]


command_urls = [
    path('command/new', views.new_command, name='new_command'),
    path('command/id=<int:id>/cancel', views.cancel_command, name='cancel_command'),
    path('command/id=<int:id>/show', views.one_command, name='one_command'),
    path('command/id=<int:id>/edit', views.edit_command, name='edit_command'),
]

"""    done  """

article_urls = [
    path('', views.index, name='index'),
    path('article/type_id/<int:object_type>/list', views.list_article_with_type, name='list_article_with_type'),
    path('article/new', views.new_article, name='new_article'),
    path('article/id=<int:id>/delete', views.delete_article, name='delete_article'),
    path('article/id=<int:id>/show', views.one_article, name='one_article'),
    path('article/id=<int:id>/show-to-rent', views.one_article_rent, name='one_article_rent'),
    path('article/id=<int:id>/edit', views.edit_article, name='edit_article'),
    path('article/id=<int:id>/doublicat', views.doublicat_article, name='doublicat_article'),
    path('article/edit/image/id=<int:image_id>/delete', views.delete_image, name='delete_image'),
    path('article/id=<int:article_id>/edit/details/id=<int:detail_id>/delete', views.delete_detail, name='delete_detail'),
    path('article/sorted/q=<str:q>', views.list_article_sorted, name='list_article_sorted'),
    path('article/sorted/tag=<int:tag>', views.get_article_by_tag, name='get_article_by_tag'),
    path('article/search/', views.search_with_query, name='search_with_query'),
    path('article/sorted/compnay=<int:id>/all', views.get_article_by_compnay, name='get_article_by_compnay'),
    path('article/one/id=<int:id>/archiving/set-visible/<str:action>', views.archiving_article, name='archiving_article'),
    path('article/one/id=<int:id>/similaires', views.article_similaires, name='article_similaires')
]

"""    done  """

archived_urls = [
    path('articles/archived', views.articles_archived, name='articles_archived'),
    path('repairs/archived', views.repair_archived, name='repair_archived'),
    path('orders/archived', views.orders_archived, name='orders_archived'),
    path('rents/archived', views.rents_archived, name='rents_archived'),
    path('mails/archived', views.mails_archived, name='mails_archived'),
    path('notes/archived', views.notes_archived, name='notes_archived'),
    path('problems/archived', views.problems_archived, name='problems_archived'),
    path('messages/archived', views.messages_archived, name='messages_archived'),
]


auth_urls = [
    path('auth', views.auth, name='auth'),
    path('logout', views.logout_user, name='logout_user'),
]


sell_urls = [
    path('sell/one/id=<int:id>/show', views.one_sell, name='one_sell'),
    path('sell/one/<int:number>/edit', views.edit_sell, name='edit_sell'),
    path('sell/one/<int:number>/add-tracking-number', views.add_tracking_number_to_sell, name='add_tracking_number_to_sell'),
    path('sell/one/<int:number>/delete', views.delete_sell, name='delete_sell'),
    path('sell/one/<int:number>/finish', views.finish_sell, name='finish_sell'),
    path('sell/one/<int:number>/add-iban', views.set_sell_iban, name='set_sell_iban'),
    path('sell/create', views.new_sell, name='new_sell'),
    path('sell/presentation', views.sell_presentation, name='sell_presentation'),

]

handels_urls = [
    path('get_pdf', views.get_pdf, name='get_pdf'),
    path('get_pdf_test', views.get_pdf_test, name='get_pdf_test'),
    path('404', views.page_404, name='page_404'),
    path('page-not-found', views.page_not_yet, name='page_not_yet'),
    path('index', views.index_user, name='index_user'),
    path('about-us', views.about_us, name='about_us'),
    path('privacy', views.privacy, name='privacy'),
    path('legal-notice', views.legal_notice, name='legal_notice'),
    path('contact-us', views.contact_us, name='contact_us'),
    # path('auto__delete_repair', views.auto__delete_repair, name='auto__delete_repair'),

]

repair_urls = [
    path('repair/presentation', views.repair_presentation, name='repair_presentation'),
    path('repair/new', views.repair_new, name='repair_new'),
    path('repair/one/id=<int:id>/', views.repair, name='repair'),
    # path('repair/one/number?<int:number>/', views.repair, name='repair'),
    path('repair/one/id=<int:id>/add-tracking-number', views.repair_tracking_number, name='repair_tracking_number'),
    path('repair/one/id=<int:id>/delete', views.delete_repair, name='delete_repair'),
    path('repair/one/id=<int:id>/finished', views.repair_finished, name='repair_finished'),
    # path('repair/one/id=<int:id>/leave-review', views.leave_review_for_repair, name='leave_review_for_repair'),
    path('repair/one/id=<int:id>/archiving/set-visible/<str:action>', views.archiving_repair, name='archiving_repair'),
    path('repair/activdes/all', views.list_repairs, name='list_repairs'),
    path('repair/all', views.list_repairs_all, name='list_repairs_all'),
    path('myrepairs/all', views.list_my_repairs, name='list_my_repairs'),
    path('repairs/all/sorted', views.sort_repair_by_date, name='sort_repair_by_date'),

]
rent_urls = [
    path('rent/presentation', views.rent_presentation, name='rent_presentation'),
    path('rent-request/article/id=<int:id>', views.new_rent, name='new_rent'),
    path('rent-request/all', views.list_rent, name='list_rent'),
    path('rent/id=<int:id>/show', views.one_rent, name='one_rent'),
    path('article/id=<int:id>/rend', views.new_rent, name='new_rent'),
    path('rent/one/id=<int:id>/archiving/set-visible/<str:action>', views.archiving_rent, name='archiving_rent'),
    path('rent/one/id=<int:id>/close', views.close_rent, name='close_rent'),
]
account_urls = [
    path('user/account/profile/info/show', views.user_profile, name='user_profile'),
    path('user/account/profile/info/edit', views.edit_profile, name='edit_profile'),
    path('user/account/profile/address/edit', views.edit_address, name='edit_address'),
    path('user/account/profile/password/edit', views.edit_password, name='edit_password'),

    path('user/account/profile/my-basket', views.mybasket, name='mybasket'),
    path('user/account/profile/my-favorites', views.myfavorites, name='myfavorites'),
    path('user/account/profile/my-repairs', views.myrepairs, name='myrepairs'),
    path('user/account/profile/my-sells', views.mysells, name='mysells'),
    path('user/account/profile/my-orders', views.myorders, name='myorders'),
    path('user/account/profile/my-rented', views.myrented, name='myrented'),

]


basket_url = [
    path('article/id=<int:id>/add-to-basket', views.add_article_to_basket, name='add_article_to_basket'),
    path('article/id=<int:id>/delete-from-basket', views.delete_article_from_basket, name='delete_article_from_basket'),
    path('article/id=<int:id>/in-basket/quantity/set-action/action=<str:action>', views.set_quantity_in_basket, name='set_quantity_in_basket'),
]
favorites_url = [
    path('article/id=<int:id>/add-to-favorites', views.add_article_to_favorites, name='add_article_to_favorites'),
    path('article/id=<int:id>/delete-from-favorites', views.delete_article_from_favorites, name='delete_article_from_favorites'),
    path('init', views.init, name='init'),
]






urlpatterns = []
urls_list = [auth_urls, handels_urls, repair_urls, repair_urls, account_urls, basket_url, favorites_url, dronsAdmin_urls, article_urls, rent_urls, archived_urls, sell_urls]
for i in urls_list:
    urlpatterns += i

# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)